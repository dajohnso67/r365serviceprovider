using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.EventGrid.Models;
using Moq;
using R365.API.Commands.SP;
using R365.API.Commands.SPDocument;
using R365.API.Controllers.DtpAging;
using R365.API.DtpAging;
using R365.Commands;
using R365.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using R365.API.Gateway;
using Xunit;
using static R365.API.Commands.SP.Create;
using static R365.API.Commands.SPDocument.Create;
using Newtonsoft.Json;

namespace R365.Test
{
    public class ServiceProviderTest
    {
        private string InMemConnString { get; set; }
        private Guid UserId { get; set; }
        private Guid UserId2 { get; set; }

        public ServiceProviderTest() {

            InMemConnString = @"Server=(localdb)\mssqllocaldb;Database=EFProviders.InMemory;Trusted_Connection=True;ConnectRetryCount=0";
            UserId = Guid.NewGuid();
            UserId2 = Guid.NewGuid();
        }

        [Fact]
        public void Gateway_Factory_Instantiates_Runtime_Defined_Command_Type_For_ServiceProviderDocument()
        {

            Dictionary<string, object> data = new Dictionary<string, object>
            {
                {
                    "envelope",

                    new ServiceProviderDocumentEnvelope
                    {
                        Catalogue = new Domain.EnvironmentCatalogue
                        {
                            CustomerName = "TestCustomer"
                        },
                        Contract = new List<SPD_Contract>() {
                            new SPD_Contract{
                                AssignedTo = null,
                                CreatedOn = DateTime.Now,
                                FileId = 123,
                                LocationId = Guid.NewGuid(),
                                ModifiedBy = null,
                                ModifiedOn = DateTime.Now,
                                NewFileName = "testfilename.pdf",
                                Status =  Common.ServiceProviderDocumentStatus.Pending,
                                UserId = Guid.NewGuid()
                            },
                            new SPD_Contract{
                                AssignedTo = null,
                                CreatedOn = DateTime.Now,
                                FileId = 45678,
                                LocationId = Guid.NewGuid(),
                                ModifiedBy = null,
                                ModifiedOn = DateTime.Now,
                                NewFileName = "testfilename1234.pdf",
                                Status =  Common.ServiceProviderDocumentStatus.Pending,
                                UserId = Guid.NewGuid()
                            }
                        }
                    }
                }
            };

            EventGridEvent _event = new EventGridEvent
            {
                EventType = "R365.API.Commands.SPDocument.Create.CreateServiceProviderDocumentCommand",
                Data = data["envelope"],
                Topic = "test.topic",
                DataVersion = "1.0",
                EventTime = DateTime.Now,
                Subject = "Test.subject",
                Id = "1"
            };

            var _request = _event.CreateTestRequest<IRequest>();

            Assert.IsAssignableFrom<IRequest>(_request);
            Assert.IsType<CreateServiceProviderDocumentCommand>(_request);
        }

        [Fact]
        public void Gateway_Factory_Instantiates_Runtime_Defined_Command_Type()
        {
            EventGridEvent _event = new EventGridEvent
            {
                EventType = "R365.API.Commands.SP.Create.CreateServiceProviderCommand",
                Data = new ServiceProviderEnvelope
                {
                    Catalogue = new Domain.EnvironmentCatalogue
                    {
                        CustomerName = "TestCustomer"
                    },
                    Contract = new SP_Contract
                    {
                        ServiceProviderId = 0,
                        ServiceProviderName = "test provider"
                    }
                },
                Topic = "test.topic",
                DataVersion = "1.0",
                EventTime = DateTime.Now,
                Subject = "Test.subject",
                Id = "1"

            };

            var _request = _event.CreateTestRequest<IRequest>();

            Assert.IsAssignableFrom<IRequest>(_request);
            Assert.IsType<CreateServiceProviderCommand>(_request);
        }

        [Fact]
        public async Task Get_EndpointsReturnSuccessAndCorrectContentType()
        {
            var mediator = new Mock<IMediator>();

            DtpAgingQueryAsync _aging = new DtpAgingQueryAsync
            {
                ServiceProviderId = 1,
                EmailAddress = null,
                UserLogin = null
            };
            
            DtpAgingController _controller = new DtpAgingController(mediator.Object);
            IActionResult okResult = await _controller.Post(_aging) as IActionResult;

            Assert.IsType<ObjectResult>(okResult);
        }

        [Fact]
        public async Task Create_New_ServiceProvider_And_Return_List()
        {
            DataContext _context = new DataContext(InMemConnString);

            ServiceProvider sp = new ServiceProvider()
            {
                ServiceProviderId = 0,
                ServiceProviderName = "new provider"
            };

            //Act
            await _context.AddAsync<ServiceProvider>(sp);
            _context.SaveChanges(true);

            var dset = _context.ServiceProvider.Where(c => c.ServiceProviderName == "new provider").Select(x => x).ToList();

            Assert.NotEmpty(dset);
            Assert.IsType<List<ServiceProvider>>(dset);
        }

        [Fact]
        public async Task Edit_Existing_ServiceProvider_And_Return_New_Value()
        {
            DataContext _context = new DataContext(InMemConnString);

            ServiceProvider sp = new ServiceProvider()
            {
                ServiceProviderId = 1,
                ServiceProviderName = "Provider 1"
            };

            //Act
            await _context.AddAsync<ServiceProvider>(sp);
            _context.SaveChanges(true);

            sp.ServiceProviderName = "Provider 2";

            _context.Update<ServiceProvider>(sp);
            _context.SaveChanges(true);

            ServiceProvider dset = _context.ServiceProvider.Where(c => c.ServiceProviderName == "Provider 2").SingleOrDefault();

            //assert
            Assert.Equal("Provider 2", dset.ServiceProviderName);
            Assert.IsType<ServiceProvider>(dset);
        }

        [Fact]
        public async Task Create_New_ServiceProviderCustomer_And_Return_List()
        {
            DataContext _context = new DataContext(InMemConnString);

            ServiceProviderCustomer spc = new ServiceProviderCustomer()
            {
                ServiceProviderId = 1,
                CustomerName = "newcustomer",
                SPEnabled = true,
                Id = null,
                Url = "http://newcustomer.restaurant365.net"
            };

            //Act
            await _context.AddAsync<ServiceProviderCustomer>(spc);
            _context.SaveChanges(true);

            var dset = _context.ServiceProviderCustomer.Where(c => c.CustomerName == "newcustomer").Select(x => x).ToList();

            Assert.NotEmpty(dset);
            Assert.IsType<List<ServiceProviderCustomer>>(dset);
        }

        [Fact]
        public async Task Edit_Existing_ServiceProviderCustomer_And_Return_New_Value()
        {
            DataContext _context = new DataContext(InMemConnString);

            ServiceProviderCustomer spc = new ServiceProviderCustomer()
            {
                ServiceProviderId = 1,
                CustomerName = "newcustomer_test",
                SPEnabled = true,
                Url = "http://newcustomer.r365.net"
            };

            //Act
            await _context.AddAsync<ServiceProviderCustomer>(spc);
            _context.SaveChanges(true);

            spc.SPEnabled = false;

            _context.Update<ServiceProviderCustomer>(spc);
            _context.SaveChanges(true);

            ServiceProviderCustomer dset = _context.ServiceProviderCustomer.Where(c => c.CustomerName == "newcustomer_test").SingleOrDefault();

            //assert
            Assert.Equal(false, dset.SPEnabled);
            Assert.IsType<ServiceProviderCustomer>(dset);
        }


        [Fact]
        public async Task Create_New_ServiceProviderCustomerUsers_And_Return_List()
        {
            DataContext _context = new DataContext(InMemConnString);

            ServiceProviderCustomerUser[] spcu = new ServiceProviderCustomerUser[] {
                new ServiceProviderCustomerUser()
                {
                    CustomerName = "customer1",
                    LocationId = Guid.NewGuid(),
                    UserId = UserId2
                },
                new ServiceProviderCustomerUser()
                {
                    CustomerName = "customer2",
                    LocationId = Guid.NewGuid(),
                    UserId = UserId2
                }
            };

            //Act
            await _context.ServiceProviderCustomerUser.AddRangeAsync(spcu);
            _context.SaveChanges(true);

            var dset = _context.ServiceProviderCustomerUser.ToList();

            //assert
            Assert.NotEmpty(dset);
            Assert.IsType<List<ServiceProviderCustomerUser>>(dset);
        }


        [Fact]
        public async Task Edit_New_ServiceProviderCustomerUsers_And_Return_List()
        {

            DataContext _context = new DataContext(InMemConnString);

            ServiceProviderCustomerUser[] spcu = new ServiceProviderCustomerUser[] {
                    new ServiceProviderCustomerUser()
                    {
                        CustomerName = "customer1",
                        LocationId = Guid.NewGuid(),
                        UserId = UserId
                    },
                    new ServiceProviderCustomerUser()
                    {
                        CustomerName = "customer2",
                        LocationId = Guid.NewGuid(),
                        UserId = UserId
                    }
                };

            //Act
            await _context.ServiceProviderCustomerUser.AddRangeAsync(spcu);
            _context.SaveChanges(true);

            var _custUsers = _context.ServiceProviderCustomerUser.Where(c => c.UserId == UserId).Select(v => v);
            _context.RemoveRange(_custUsers);

            ServiceProviderCustomerUser[] spcu2 = new ServiceProviderCustomerUser[] {

                    new ServiceProviderCustomerUser()
                    {
                        CustomerName = "customer4",
                        LocationId = Guid.NewGuid(),
                        UserId = UserId
                    },
                    new ServiceProviderCustomerUser()
                    {
                        CustomerName = "customer5",
                        LocationId = Guid.NewGuid(),
                        UserId = UserId
                    },
                    new ServiceProviderCustomerUser()
                    {
                        CustomerName = "customer6",
                        LocationId = Guid.NewGuid(),
                        UserId = UserId
                    },
                    new ServiceProviderCustomerUser()
                    {
                        CustomerName = "customer7",
                        LocationId = Guid.NewGuid(),
                        UserId = UserId
                    }
                };

            //Act
            await _context.ServiceProviderCustomerUser.AddRangeAsync(spcu2);
            _context.SaveChanges(true);

            var _custUsers2 = _context.ServiceProviderCustomerUser.Where(c => c.UserId == UserId).Select(v => v).ToList();

            var dset = _context.ServiceProviderCustomerUser.ToList();

            //assert
            Assert.Equal(4, _custUsers2.Count());
            Assert.IsType<List<ServiceProviderCustomerUser>>(dset);
        }


        [Fact]
        public async Task CreateServiceProviderDocumentCommand_MediatR_Test_Send()
        {

            DataContext _context = new DataContext(InMemConnString);

            R365.API.Commands.SPDocument.Create.CreateServiceProviderDocumentCommand cmd = new API.Commands.SPDocument.Create.CreateServiceProviderDocumentCommand();
            _ = new API.Commands.SPDocument.Create.Handler(_context);
            cmd.Envelope = new API.Commands.SPDocument.ServiceProviderDocumentEnvelope();
            cmd.Envelope.Contract = new List<SPD_Contract>() {

                new API.Commands.SPDocument.SPD_Contract()

            };

            var mediator = new Mock<IMediator>();

            Unit ret = await mediator.Object.Send(cmd);

            Assert.IsType<Unit>(ret);

        }

        [Fact]
        public async Task UpdateServiceProviderDocumentCommand_MediatR_Test_Send()
        {

            DataContext _context = new DataContext(InMemConnString);

            R365.API.Commands.SPDocument.Edit.UpdateServiceProviderDocumentCommand cmd = new API.Commands.SPDocument.Edit.UpdateServiceProviderDocumentCommand();
            _ = new API.Commands.SPDocument.Edit.Handler(_context);
            cmd.Envelope = new API.Commands.SPDocument.ServiceProviderDocumentEnvelope();
            cmd.Envelope.Contract = new List<SPD_Contract>() { new API.Commands.SPDocument.SPD_Contract() };

            var mediator = new Mock<IMediator>();

            Unit ret = await mediator.Object.Send(cmd);

            Assert.IsType<Unit>(ret);

        }
    }
}

