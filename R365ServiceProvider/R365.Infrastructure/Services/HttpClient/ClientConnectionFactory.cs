﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace R365.Infrastructure.Http
{
    /// <summary>
    /// ClientConnectionFactory
    /// </summary>
    public class ClientConnectionFactory : IClientConnectionFactory
    {
        public ClientConnectionFactory() { }

        private static readonly IDictionary<string, HttpClient> clientCache = new Dictionary<string, HttpClient>();

        /// <summary>
        /// Create
        /// </summary>
        /// <param name="endpoint"></param>
        /// <returns></returns>
        public HttpClient Create(string endpoint)
        {
            if (clientCache.TryGetValue(endpoint, out HttpClient client))
            {
                return client;
            }

            Uri baseUri = new Uri(endpoint);

            var handler = new SocketsHttpHandler
            {
                MaxConnectionsPerServer = int.MaxValue,
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };

            client = new HttpClient(handler)
            {
                BaseAddress = baseUri
            };

            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.ConnectionClose = false;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            ServicePointManager.FindServicePoint(baseUri).ConnectionLeaseTimeout = 60 * 1000;

            clientCache[endpoint] = client;

            return client;
        }
    }
}
