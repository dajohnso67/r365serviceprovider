﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace R365.Infrastructure.Http
{
    public interface IClientConnectionFactory
    {
        HttpClient Create(string endpoint);
    }
}
