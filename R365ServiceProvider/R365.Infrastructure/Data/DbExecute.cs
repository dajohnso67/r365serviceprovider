﻿﻿using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using Dapper;
using System.Data;
using System;
using System.Threading.Tasks;
using System.Dynamic;
using R365.Common;


namespace R365.Infrastructure.Data
{

    public class DbExecute
    {

        public EnvironmentType EnvironmentType { get; set; }

        private ConnectionStringProvider _connectionStrings { get; set; }
        private string ConnectionString { get; set; }
        

        public DbExecute(ConnectionStringProvider connectionStrings) {

            _connectionStrings = connectionStrings;

        }

        /// <summary>
        /// ExecuteReaderAsync
        /// </summary>
        /// <param name="storedProcedure"></param>
        /// <param name="sql"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// 
        /// <returns>Task<IDataReader></returns>
        public async Task<IDataReader> ExecuteReaderAsync(string storedProcedure, SqlParamCollection sql, SqlTransaction transaction = null, int? commandTimeout = 0)
        {

            ConnectionString = _connectionStrings.GetEnvironmentConnectionString(EnvironmentType);

            IDbConnection connection = new SqlConnection(ConnectionString);
            DynamicParameters dp = sql.ConvertToDynamicParameters();

            connection.Open();
            IDataReader dr = await connection.ExecuteReaderAsync(storedProcedure, param: dp, transaction: transaction, commandTimeout: commandTimeout, commandType: CommandType.StoredProcedure);


            sql.SetOutputParams(dp);

            return dr;
        }


        /// <summary>
        /// QueryMultipleAsync
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="storedProcedure"></param>
        /// <param name="sql"></param>
        /// <param name="transaction"></param>
        /// <param name="buffered"></param>
        /// <param name="commandTimeout"></param>
        /// <returns>Task<Tuple<IEnumerable<T1>, IEnumerable<T2>>></returns>
        public async Task<Tuple<List<T1>, List<T2>>> QueryMultipleAsync<T1, T2>(string storedProcedure, SqlParamCollection sql, SqlTransaction transaction = null, int? commandTimeout = 0)
            where T1 : class
            where T2 : class
        {

            DynamicParameters dp = sql.ConvertToDynamicParameters();

            Tuple<List<T1>, List<T2>> returnVal = null;

            ConnectionString = _connectionStrings.GetEnvironmentConnectionString(EnvironmentType);

            using (IDbConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                Task<SqlMapper.GridReader> multi = connection.QueryMultipleAsync(storedProcedure, param: dp, transaction: transaction, commandTimeout: commandTimeout, commandType: CommandType.StoredProcedure);

                sql.SetOutputParams(dp);

                var r1 = await multi.Result.ReadAsync<T1>();
                var r2 = await multi.Result.ReadAsync<T2>();

                returnVal = new Tuple<List<T1>, List<T2>>(r1.ToList<T1>(), r2.ToList<T2>());

            }

            return returnVal;
        }


        /// <summary>
        /// QuerySingleAsync
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storedProcedure"></param>
        /// <param name="sql"></param>
        /// <param name="transaction"></param>
        /// <param name="buffered"></param>
        /// <param name="commandTimeout"></param>
        /// <returns>Task<T></returns>
        public async Task<T> QuerySingleAsync<T>(string storedProcedure, CommandType commandType, object param = null) where T : class
        {

            ConnectionString = _connectionStrings.GetEnvironmentConnectionString(EnvironmentType);

            T t = null;

            using (IDbConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                t = await connection.QuerySingleAsync<T>(storedProcedure, param: param, commandType: commandType);

            }

            return t;
        }


        /// <summary>
        /// QuerySingleAsync
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storedProcedure"></param>
        /// <param name="sql"></param>
        /// <param name="transaction"></param>
        /// <param name="buffered"></param>
        /// <param name="commandTimeout"></param>
        /// <returns>Task<T></returns>
        public async Task<T> QuerySingleAsync<T>(string storedProcedure, SqlParamCollection sql, SqlTransaction transaction = null, int? commandTimeout = 0) where T : class
        {
 
            DynamicParameters dp = sql.ConvertToDynamicParameters();

            ConnectionString = _connectionStrings.GetEnvironmentConnectionString(EnvironmentType);

            T t = null;

            using (IDbConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                t = await connection.QuerySingleAsync<T>(storedProcedure, param: dp, transaction: transaction, commandTimeout: commandTimeout, commandType: CommandType.StoredProcedure);

                sql.SetOutputParams(dp);
            }

            return t;
        }

        /// <summary>
        /// QueryFirstOrDefaultAsync
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storedProcedure"></param>
        /// <param name="sql"></param>
        /// <param name="transaction"></param>
        /// <param name="buffered"></param>
        /// <param name="commandTimeout"></param>
        /// <returns>Task<T></returns>
        public async Task<T> QueryFirstOrDefaultAsync<T>(string storedProcedure, SqlParamCollection sql, SqlTransaction transaction = null, int? commandTimeout = 0) where T : class
        {

            DynamicParameters dp = sql.ConvertToDynamicParameters();

            ConnectionString = _connectionStrings.GetEnvironmentConnectionString(EnvironmentType);

            T t = null;

            using (IDbConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                t = await connection.QueryFirstOrDefaultAsync<T>(storedProcedure, param: dp, transaction: transaction, commandTimeout: commandTimeout, commandType: CommandType.StoredProcedure);

                sql.SetOutputParams(dp);
            }

            return t;
        }

        /// <summary>
        /// QueryAsync
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storedProcedure"></param>
        /// <param name="sql"></param>
        /// <param name="transaction"></param>
        /// <param name="commandTimeout"></param>
        /// 
        /// <returns>Task<IEnumerable<T>></returns>
        public async Task<List<T>> QueryAsync<T>(string storedProcedure, SqlParamCollection sql, SqlTransaction transaction = null, int? commandTimeout = 0) where T : class
        {

            IEnumerable<T> t = null;
            DynamicParameters dp = sql.ConvertToDynamicParameters();

            ConnectionString = _connectionStrings.GetEnvironmentConnectionString(EnvironmentType);

            using (IDbConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                t = await connection.QueryAsync<T>(storedProcedure, param: dp, transaction: transaction, commandTimeout: commandTimeout, commandType: CommandType.StoredProcedure);
                sql.SetOutputParams(dp);

            }

            return t.ToList();
        }



        /// <summary>
        /// QueryAsync
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storedProcedure"></param>
        /// <param name="sql"></param>
        /// <param name="transaction"></param>
        /// <param name="buffered"></param>
        /// <param name="commandTimeout"></param>
        /// <returns>Task<IEnumerable<T>></returns>
        public async Task<List<T>> QueryAsync<T>(string storedProcedure, CommandType commandType, object param = null, SqlTransaction transaction = null, int? commandTimeout = 0) where T : class
        {

            IEnumerable<T> t = null;
    
            ConnectionString = _connectionStrings.GetEnvironmentConnectionString(EnvironmentType);

            using (IDbConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                t = await connection.QueryAsync<T>(storedProcedure, param: param, transaction: transaction, commandTimeout: commandTimeout, commandType: commandType);

            }

            return t.ToList<T>();
        }



        /// <summary>
        /// Query
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storedProcedure"></param>
        /// <param name="sql"></param>
        /// <param name="transaction"></param>
        /// <param name="buffered"></param>
        /// <param name="commandTimeout"></param>
        /// <returns>List<T></returns>
        public List<T> Query<T>(string storedProcedure, SqlParamCollection sql, SqlTransaction transaction = null, int? commandTimeout = 0) where T : class
        {

            DynamicParameters dp = sql.ConvertToDynamicParameters();

            ConnectionString = _connectionStrings.GetEnvironmentConnectionString(EnvironmentType);

            List<T> t = null;

            using (IDbConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                t = connection.Query<T>(storedProcedure, param: dp, transaction: transaction, commandTimeout: commandTimeout, commandType: CommandType.StoredProcedure).ToList<T>();
                sql.SetOutputParams(dp);
            }

            return t;
        }

        /// <summary>
        /// ExecuteScalarAsync
        /// </summary>
        /// <param name="storedProcedure"></param>
        /// <param name="param"></param>
        /// <param name="transaction"></param>
        /// <param name="buffered"></param>
        /// <param name="commandTimeout"></param>
        /// <returns>Task<object></returns>
        public async Task<object> ExecuteScalarAsync(string storedProcedure, SqlParamCollection sql, SqlTransaction transaction = null, int? commandTimeout = 0)
        {

            DynamicParameters dp = sql.ConvertToDynamicParameters();

            ConnectionString = _connectionStrings.GetEnvironmentConnectionString(EnvironmentType);

            object returnVal = null;

            using (IDbConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                returnVal = await connection.ExecuteScalarAsync(storedProcedure, param: dp, transaction: transaction, commandTimeout: commandTimeout, commandType: CommandType.StoredProcedure);
                sql.SetOutputParams(dp);
            }

            return returnVal;
        }

        /// <summary>
        /// ExecuteScalar
        /// </summary>
        /// <param name="storedProcedure"></param>
        /// <param name="param"></param>
        /// <param name="transaction"></param>
        /// <param name="buffered"></param>
        /// <param name="commandTimeout"></param>
        /// <returns>object</returns>
        public object ExecuteScalar(string storedProcedure, SqlParamCollection sql, SqlTransaction transaction = null, int? commandTimeout = 0)
        {

            DynamicParameters dp = sql.ConvertToDynamicParameters();

            ConnectionString = _connectionStrings.GetEnvironmentConnectionString(EnvironmentType);


            object returnVal = null;

            using (IDbConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                returnVal = connection.ExecuteScalar(storedProcedure, param: dp, transaction: transaction, commandTimeout: commandTimeout, commandType: CommandType.StoredProcedure);
                sql.SetOutputParams(dp);
            }

            return returnVal;
        }

        /// <summary>
        /// ExecuteScalarAsync
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storedProcedure"></param>
        /// <param name="param"></param>
        /// <param name="transaction"></param>
        /// <param name="buffered"></param>
        /// <param name="commandTimeout"></param>
        /// <returns>Task<T></returns>
        public async Task<T> ExecuteScalarAsync<T>(string storedProcedure, SqlParamCollection sql, SqlTransaction transaction = null, int? commandTimeout = 0)
        {

            DynamicParameters dp = sql.ConvertToDynamicParameters();

            ConnectionString = _connectionStrings.GetEnvironmentConnectionString(EnvironmentType);

            T val = default(T);

            using (IDbConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                val = await connection.ExecuteScalarAsync<T>(storedProcedure, param: dp, transaction: transaction, commandTimeout: commandTimeout, commandType: CommandType.StoredProcedure);
                sql.SetOutputParams(dp);
            }

            if (!(val is DBNull))
            {
                return (T)val;
            }
            else
            {
                return default(T);
            }

        }

        /// <summary>
        /// ExecuteScalar
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storedProcedure"></param>
        /// <param name="param"></param>
        /// <param name="transaction"></param>
        /// <param name="buffered"></param>
        /// <param name="commandTimeout"></param>
        /// <returns>T</returns>
        public T ExecuteScalar<T>(string storedProcedure, SqlParamCollection sql, SqlTransaction transaction = null, int? commandTimeout = 0)
        {

            DynamicParameters dp = sql.ConvertToDynamicParameters();

            ConnectionString = _connectionStrings.GetEnvironmentConnectionString(EnvironmentType);

            T val = default;

            using (IDbConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                val = connection.ExecuteScalar<T>(storedProcedure, param: dp, transaction: transaction, commandTimeout: commandTimeout, commandType: CommandType.StoredProcedure);
                sql.SetOutputParams(dp);
            }

            if (!(val is DBNull))
            {
                return (T)val;
            }
            else
            {
                return default;
            }

        }

        /// <summary>
        /// ExecuteNonQueryAsync
        /// </summary>
        /// <param name="storedProcedure"></param>
        /// <param name="param"></param>
        /// <param name="transaction"></param>
        /// <param name="buffered"></param>
        /// <param name="commandTimeout"></param>
        /// <returns>Task<int></returns>
        public async Task<int> ExecuteNonQueryAsync(string storedProcedure, SqlParamCollection sql, SqlTransaction transaction = null, int? commandTimeout = 0)
        {

            DynamicParameters dp = sql.ConvertToDynamicParameters();

            ConnectionString = _connectionStrings.GetEnvironmentConnectionString(EnvironmentType);

            int ret = 0;

            using (IDbConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                ret = await connection.ExecuteAsync(storedProcedure, param: dp, transaction: transaction, commandTimeout: commandTimeout, commandType: CommandType.StoredProcedure);
                sql.SetOutputParams(dp);
            }

            return ret;
        }

        /// <summary>
        /// ExecuteNonQuery
        /// </summary>
        /// <param name="storedProcedure"></param>
        /// <param name="param"></param>
        /// <param name="transaction"></param>
        /// <param name="buffered"></param>
        /// <param name="commandTimeout"></param>
        /// <returns>int?</returns>
        public int? ExecuteNonQuery(string storedProcedure, SqlParamCollection sql, SqlTransaction transaction = null, int? commandTimeout = 0)
        {

            DynamicParameters dp = sql.ConvertToDynamicParameters();

            ConnectionString = _connectionStrings.GetEnvironmentConnectionString(EnvironmentType);

            int? returnVal = null;

            using (IDbConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                returnVal = connection.Execute(storedProcedure, param: dp, transaction: transaction, commandTimeout: commandTimeout, commandType: CommandType.StoredProcedure);
                sql.SetOutputParams(dp);
            }

            return returnVal;
        }
    }



    public class DynamicObject
    {
        public dynamic Instance = new ExpandoObject();

        /// <summary>
        /// AddProperty
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public void AddProperty(string name, object value)
        {
            ((IDictionary<string, object>)this.Instance).Add(name, value);
        }

        /// <summary>
        /// GetProperty
        /// </summary>
        /// <param name="name"></param>
        /// <returns>dynamic</returns>
        public dynamic GetProperty(string name)
        {
            if (((IDictionary<string, object>)this.Instance).ContainsKey(name))
                return ((IDictionary<string, object>)this.Instance)[name];
            else
                return null;
        }
    }

    public static class SqlParamConvert
    {
        /// <summary>
        /// ConvertToDynamicParameters
        /// </summary>
        /// <param name="pc"></param>
        /// <returns>DynamicParameters</returns>
        public static DynamicParameters ConvertToDynamicParameters(this SqlParamCollection pc)
        {
            DynamicParameters p = new DynamicParameters();

            foreach (var item in pc)
            {
                p.Add(name: item.Name, value: item.Value, dbType: item.DataType, direction: item.Direction, size: item.Size, precision: item.Precision, scale: item.Scale);
            }

            return p;
        }
    }


}




