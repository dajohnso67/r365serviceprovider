﻿using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace R365.Infrastructure.Data
{
    public class SqlParamCollection : IEnumerable<SqlParam>
    {
        private readonly List<SqlParam> _params;

        public SqlParamCollection()
        {
            _params = new List<SqlParam>();
        }

        /// <summary>
        /// Add
        /// </summary>
        /// <param name="name"></param>
        /// <param name="datatype"></param>
        /// <param name="direction"></param>
        public void Add(string name, DbType? datatype, ParameterDirection? direction)
        {
            _params.Add(new SqlParam(name, null, datatype, direction, null));
        }

        /// <summary>
        /// Add
        /// </summary>
        /// <param name="name"></param>
        /// <param name="datatype"></param>
        /// <param name="direction"></param>
        /// <param name="size"></param>
        public void Add(string name, DbType? datatype, ParameterDirection? direction, int? size)
        {
            _params.Add(new SqlParam(name, null, datatype, direction, size));
        }

        /// <summary>
        /// Add
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="datatype"></param>
        /// <param name="direction"></param>
        public void Add(string name, object value, DbType? datatype, ParameterDirection? direction)
        {
            _params.Add(new SqlParam(name, value, datatype, direction, null));
        }

        /// <summary>
        /// Add
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="datatype"></param>
        /// <param name="direction"></param>
        /// <param name="size"></param>
        public void Add(string name, object value, DbType? datatype, ParameterDirection? direction, int? size)
        {
            _params.Add(new SqlParam(name, value, datatype, direction, size));
        }

        /// <summary>
        /// Add
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="datatype"></param>
        /// <param name="direction"></param>
        /// <param name="size"></param>
        /// <param name="precision"></param>
        public void Add(string name, object value, DbType? datatype, ParameterDirection? direction, int? size, byte? precision)
        {
            _params.Add(new SqlParam(name, value, datatype, direction, size, precision, null));
        }

        /// <summary>
        /// Add
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="datatype"></param>
        /// <param name="direction"></param>
        /// <param name="size"></param>
        /// <param name="precision"></param>
        /// <param name="scale"></param>
        public void Add(string name, object value = null, DbType? datatype = null, ParameterDirection? direction = null, int? size = null, byte? precision = null, byte? scale = null)
        {
            _params.Add(new SqlParam(name, value, datatype, direction, size, precision, scale));
        }

        public IEnumerator<SqlParam> GetEnumerator()
        {
            return _params.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<SqlParam>)_params).GetEnumerator();
        }
        /// <summary>
        /// SetOutputParams
        /// </summary>
        /// <param name="dp"></param>
        public void SetOutputParams(DynamicParameters dp)
        {
            foreach (SqlParam item in _params)
            {
                if (item.Direction == ParameterDirection.Output)
                {
                    item.Value = dp.Get<object>(item.Name);
                }

                if (item.Direction == ParameterDirection.ReturnValue)
                {
                    item.ReturnValue = dp.Get<object>(item.Name);
                }
            }
        }

        /// <summary>
        /// GetOutputParam
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public T GetOutputParam<T>(string name)
        {
            SqlParam sqlParam = _params.Where(p => p.Direction == ParameterDirection.Output && p.Name == name).FirstOrDefault();
            return (T)Convert.ChangeType(sqlParam.Value, typeof(T));
        }

        /// <summary>
        /// GetReturnValue
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        //public T GetReturnValue<T>(string name)
        //{
        //    SqlParam sqlParam = _params.Where(p => p.Direction == ParameterDirection.ReturnValue && p.Name == name).FirstOrDefault();
        //    return (T)Convert.ChangeType(sqlParam.ReturnValue, typeof(T));
        //}

        public T GetReturnValue<T>()
        {
            SqlParam sqlParam = _params.Where(p => p.Direction == ParameterDirection.ReturnValue).FirstOrDefault();

            T val = (T)Convert.ChangeType(sqlParam.ReturnValue, typeof(T));

            return val;
        }
    }

    public class SqlParam
    {
        public SqlParam() { }

        /// <summary>
        /// SqlParam
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="datatype"></param>
        /// <param name="direction"></param>
        /// <param name="size"></param>
        public SqlParam(string name, object value, DbType? datatype, ParameterDirection? direction, int? size)
        {
            Name = name;
            Value = value;
            DataType = datatype;
            Direction = direction;
            Size = size;
        }

        /// <summary>
        /// SqlParam
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="datatype"></param>
        /// <param name="direction"></param>
        /// <param name="size"></param>
        /// <param name="precision"></param>
        /// <param name="scale"></param>
        public SqlParam(string name, object value, DbType? datatype, ParameterDirection? direction, int? size, byte? precision, byte? scale)
        {
            Name = name;
            Value = value;
            DataType = datatype;
            Direction = direction;
            Size = size;
            Precision = precision;
            Scale = scale;
        }

        public string Name { get; set; }
        public object Value { get; set; }
        public object ReturnValue { get; set; }
        public DbType? DataType { get; set; }
        public ParameterDirection? Direction { get; set; }
        public int? Size { get; set; }
        public byte? Precision { get; set; }
        public byte? Scale { get; set; }
    }
}
