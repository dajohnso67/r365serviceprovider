﻿using Microsoft.EntityFrameworkCore;
using R365.Common;
using R365.Domain;
using System;

namespace R365.Commands
{
    public class DataContext : DbContext
    {
        private readonly ConnectionStringProvider _dbConnProvider;
        private readonly string _connectionString;

        public DataContext(ConnectionStringProvider dbConnProvider) : base()
        {
            _dbConnProvider = dbConnProvider;
        }

        public DataContext(string ConnectionString) : base()
        {
            _connectionString = ConnectionString;
        }

        public DbSet<ServiceProvider>               ServiceProvider { get; set; }
        public DbSet<ServiceProviderCustomer>       ServiceProviderCustomer { get; set; }
        public DbSet<ServiceProviderUser>           ServiceProviderUser { get; set; }
        public DbSet<ServiceProviderCustomerUser>   ServiceProviderCustomerUser { get; set; }
        public DbSet<ServiceProviderDocument>       ServiceProviderDocument { get; set; }

        /// <summary>
        /// OnConfiguring
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            if (!string.IsNullOrEmpty(_connectionString))
            {
                optionsBuilder.UseInMemoryDatabase(_connectionString);
            }
            else {

                optionsBuilder.UseSqlServer(_dbConnProvider.ConnectionString, sqlServerOptionsAction: sqlOptions => {
                    sqlOptions.EnableRetryOnFailure(maxRetryCount: 10, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
                });
            }
            
            base.OnConfiguring(optionsBuilder);
        }

        /// <summary>
        /// OnModelCreating
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder){}

    }
}
