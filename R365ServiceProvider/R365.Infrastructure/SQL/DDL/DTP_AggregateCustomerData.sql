﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER     PROCEDURE [dbo].[DTP_AggregateCustomerData]   

	@DataBaseName AS VARCHAR(100) = NULL,
	@EnvironmentSuffix AS VARCHAR(20),
	@DeletePriorToInsert AS BIT = 1

AS  

	IF @DataBaseName IS NULL
	BEGIN
		SET @DataBaseName = DB_NAME();
	END

	IF @DeletePriorToInsert = 1
	BEGIN
		DELETE FROM [COMMON].[r365].[dbo].[ServiceProviderUser] WHERE CustomerName = @DataBaseName
		DELETE FROM [COMMON].[r365].[dbo].[ServiceProviderCustomerUser] WHERE CustomerName = @DataBaseName
		DELETE FROM [COMMON].[r365].[dbo].[ServiceProviderCustomer] WHERE CustomerName = @DataBaseName
	END

	--ServiceProviderCustomer
	INSERT INTO [COMMON].[r365].[dbo].[ServiceProviderCustomer](ServiceProviderId, CustomerName, [URL], SPEnabled)
	SELECT
		  1 AS ServiceProviderId
		, @DataBaseName as CustomerName
		, 'https://' + @DataBaseName + '.restaurant365.' + @EnvironmentSuffix as [URL]
		, 1 AS SPEnabled


	DECLARE @ServiceProviderCustomerUser TABLE
	(
		CustomerName	VARCHAR(200),
		LocationId		UNIQUEIDENTIFIER,
		UserId			UNIQUEIDENTIFIER
	);


	INSERT INTO @ServiceProviderCustomerUser(CustomerName, LocationId, UserId)
	SELECT 
		@DataBaseName as CustomerName,
		dm_locationId as LocationId,
		p.SystemUserId as UserId
	FROM dm_location 
	CROSS JOIN (
		SELECT SystemUserId, dm_AvailableToAllLocations, r.[Name], dm_inactive
		FROM SystemUserBase su
		JOIN UserRole ur on su.SystemUserId = ur.UserId
		JOIN Roles r on r.RoleId = ur.RoleId where dm_AvailableToAllLocations = 1 
		) p
	WHERE p.dm_AvailableToAllLocations = 1 AND p.[Name] = 'AP Data Entry' AND ISNULL(p.dm_inactive, 0) = 0
	GROUP BY p.SystemUserId, dm_locationId
	UNION ALL
	SELECT 
		@DataBaseName as CustomerName
		, ul.[Location] as LocationId
		, su.systemuserId as UserId
	FROM SystemUserBase su
		JOIN UserRole ur on su.SystemUserId = ur.UserId
		JOIN Roles r on r.RoleId = ur.RoleId
		JOIN UserLocation ul on su.SystemUserId = ul.[User]
	WHERE r.[Name] = 'AP Data Entry'
		AND (su.dm_AvailableToAllLocations = 0 OR su.dm_AvailableToAllLocations IS NULL )
		AND ISNULL(dm_inactive, 0) = 0
	GROUP BY su.SystemUserId, ul.[Location]



	--ServiceProviderCustomerUser
	INSERT INTO [COMMON].[r365].[dbo].[ServiceProviderCustomerUser](CustomerName, LocationId, UserId)
	SELECT 
	  CustomerName 
	, LocationId
	, UserId
	FROM @ServiceProviderCustomerUser
	GROUP BY CustomerName, UserId, LocationId


	--ServiceProviderUser
	INSERT INTO [COMMON].[r365].[dbo].[ServiceProviderUser] (UserId, FullName, EmailAddress, UserLogin, CustomerName)
	SELECT 
		  su.SystemUserId as UserId
		, su.FullName as FullName
		, su.InternalEMailAddress as EmailAddress
		, su.dm_Login AS UserLogin
		, @DataBaseName as CustomerName
	FROM SystemUserBase su
		INNER JOIN UserRole ur on su.SystemUserId = ur.UserId
		INNER JOIN Roles r on r.RoleId = ur.RoleId
	WHERE r.Name = 'AP Data Entry' AND ISNULL(su.dm_inactive, 0) = 0
	GROUP BY su.SystemUserId, su.FullName, su.InternalEMailAddress, su.dm_Login


	CREATE TABLE #TmpPending  
	(
			FileId int,  
			CustomerName varchar(50),  
			LocationId uniqueidentifier,  
			AssignedTo uniqueidentifier,  
			[Status] varchar(50),  
			NewFileName varchar(200),  
			CreatedOn datetime,
			ModifiedOn datetime,
			ModifiedBy uniqueidentifier,
			UserId uniqueidentifier
	);  

	CREATE TABLE #TmpComplete 
	(
		FileId int,  
		CustomerName varchar(50),  
		LocationId uniqueidentifier,  
		AssignedTo uniqueidentifier,  
		[Status] varchar(50),  
		NewFileName varchar(200),  
		CreatedOn datetime,
		ModifiedOn datetime,
		ModifiedBy uniqueidentifier,
		UserId uniqueidentifier
	); 

	INSERT INTO #TmpPending
	SELECT 
		FileId
		, @DataBaseName as CustomerName
		, ISNULL(LocationId, '00000000-0000-0000-0000-000000000000') as LocationId
		, NULL AssignedTo	--Future
		, 'Pending' [Status]
		, newFileName
		, GETDATE() as CreatedOn
		, NULL as ModifiedOn
		, NULL ModifiedBy
		, UserId
	FROM UploadAttachments 
	WHERE [Status] = 0 AND markAsDuplicate = '0'


	INSERT INTO #TmpComplete
	SELECT 
		FileId
		, @DataBaseName as CustomerName
		, ISNULL(LocationId, '00000000-0000-0000-0000-000000000000') as LocationId
		, NULL AssignedTo	--Future
		, 'Complete' [Status]
		, newFileName
		, GETDATE() as CreatedOn
		, NULL as ModifiedOn
		, NULL ModifiedBy
		, UserId
	FROM UploadAttachments 
	WHERE [Status] = 1 AND markAsDuplicate = '0'


	INSERT INTO [COMMON].[r365].[dbo].[ServiceProviderDocument](FileId, CustomerName, LocationId, AssignedTo, [Status], newFileName, CreatedOn, modifiedOn, ModifiedBy, UserId)
	SELECT s.FileId, s.CustomerName, s.LocationId, s.AssignedTo, s.[Status], s.NewFileName, s.CreatedOn, s.ModifiedOn, s.ModifiedBy, s.UserId
	FROM   #TmpPending s
	WHERE NOT EXISTS (
				SELECT t.FileId, t.LocationId, t.NewFileName, t.UserId, t.CustomerName
				FROM   [COMMON].[r365].[dbo].[ServiceProviderDocument] t
				WHERE  t.FileId = s.FileId AND t.CustomerName = s.CustomerName
			)


	--update status to complete once a document has been processed
	UPDATE [COMMON].[r365].[dbo].[ServiceProviderDocument] 
	SET [Status] = l.[Status], ModifiedOn = l.ModifiedOn
	FROM [COMMON].[r365].[dbo].[ServiceProviderDocument] y
	INNER JOIN (
		SELECT 
			s.FileId, 
			s.CustomerName,
			s.[Status], 
			ISNULL(t.ModifiedOn, GETDATE()) AS ModifiedOn
		FROM #TmpComplete s
		INNER JOIN [COMMON].[r365].[dbo].[ServiceProviderDocument]  t ON t.FileId = s.FileId AND t.CustomerName = s.CustomerName
	) l ON l.FileId = y.FileId AND l.CustomerName = y.CustomerName


	--Clean up all of the orphaned data which occures when documents are removed from the source (UploadAttachments) that have already been moved into the [ServiceProviderDocument] destination table
	DELETE y
	FROM [COMMON].[r365].[dbo].[ServiceProviderDocument]  y
	INNER JOIN (
		
		SELECT FileId, CustomerName, [status]
		FROM [COMMON].[r365].[dbo].[ServiceProviderDocument]
		WHERE CustomerName = @DataBaseName AND [status] = 'Pending' AND FileId NOT IN
		(
			SELECT spd.FileId
			FROM UploadAttachments ua
			INNER JOIN [COMMON].[r365].[dbo].[ServiceProviderDocument] spd ON spd.FileId = ua.FileId
			WHERE spd.CustomerName = @DataBaseName AND ua.[Status] = 0 AND ua.markAsDuplicate = '0'
		) 

	) l ON l.FileId = y.FileId AND l.CustomerName = y.CustomerName



	DROP TABLE #TmpPending
	DROP TABLE #TmpComplete
