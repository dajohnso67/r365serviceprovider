USE [R365]
GO
/****** Object:  StoredProcedure [dbo].[DTP_Aging_Report]    Script Date: 6/24/2019 11:57:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[DTP_Aging_Report]   

    @ServiceProviderId	AS INT = NULL,
	@EmailAddress		AS VARCHAR(320) = NULL,
	@UserLogin			AS VARCHAR(200) = NULL,
	@Userid				AS UNIQUEIDENTIFIER = NULL,
	@ShowUnassigned		AS BIT = 1
AS   
      
	BEGIN TRY

		SET NOCOUNT ON;  

		IF(@Userid IS NOT NULL)
		BEGIN
			SET @UserLogin = (SELECT TOP(1) UserLogin FROM ServiceProviderUser where UserId = @Userid)
		END

		DECLARE @dtpCount TABLE
		(
			CustomerName	VARCHAR(200),
			[Url]			VARCHAR(2083),
			_0_12			INT,
			_12_16			INT,
			_16_24			INT,
			Over_24			INT
		);

		
		CREATE TABLE #TmpOutput
		(
			CustomerName	VARCHAR(200),
			[Url]			VARCHAR(2083),
			_0_12			INT,
			_12_16			INT,
			_16_24			INT,
			Over_24			INT,
			Total			INT
		);

		CREATE CLUSTERED INDEX cx_tempusers ON #TmpOutput (Total DESC);


		CREATE TABLE #TmpDocuments 
		(
			FileId int,  
			CustomerName varchar(50),  
			LocationId uniqueidentifier,  
			AssignedTo uniqueidentifier,  
			[Status] varchar(50),  
			NewFileName varchar(200),  
			CreatedOn datetime,
			ModifiedOn datetime,
			ModifiedBy uniqueidentifier,
			UserId uniqueidentifier,
			id int
		); 

		IF @ShowUnassigned = 0
		BEGIN
			INSERT INTO #TmpDocuments(FileId, CustomerName, LocationId, AssignedTo, [Status], NewFileName, CreatedOn, ModifiedOn, ModifiedBy, UserId, Id)
			SELECT * FROM [COMMON].[r365].[dbo].[ServiceProviderDocument] WHERE LocationId != '00000000-0000-0000-0000-000000000000' 
		END

		IF @ShowUnassigned = 1
		BEGIN
			INSERT INTO #TmpDocuments(FileId, CustomerName, LocationId, AssignedTo, [Status], NewFileName, CreatedOn, ModifiedOn, ModifiedBy, UserId, Id)
			SELECT * FROM [COMMON].[r365].[dbo].[ServiceProviderDocument]
		END

		INSERT INTO @dtpCount(CustomerName,[Url], _0_12, _12_16, _16_24, Over_24)
		SELECT 
			  CustomerName
			, [Url]
			, COUNT(CASE WHEN DATEDIFF(HOUR, CreatedOn, GETDATE()) >= 0 AND DATEDIFF(hour, CreatedOn, GETDATE()) <= 12 THEN DATEDIFF(hour, CreatedOn, GETDATE()) ELSE NULL END) AS _0_12
			, COUNT(CASE WHEN DATEDIFF(HOUR, CreatedOn, GETDATE()) > 12 AND DATEDIFF(hour, CreatedOn, GETDATE()) <= 16 THEN DATEDIFF(hour, CreatedOn, GETDATE()) ELSE NULL END) AS _12_16
			, COUNT(CASE WHEN DATEDIFF(HOUR, CreatedOn, GETDATE()) > 16 AND DATEDIFF(hour, CreatedOn, GETDATE()) <= 24 THEN DATEDIFF(hour, CreatedOn, GETDATE()) ELSE NULL END) AS _16_24
			, COUNT(CASE WHEN DATEDIFF(HOUR, CreatedOn, GETDATE()) > 24 THEN DATEDIFF(hour, CreatedOn, GETDATE()) ELSE NULL END) as Over_24
		FROM 
		(
			SELECT 
				  spc.CustomerName
				, spc.[Url]
				, spd.NewFileName
				, spd.LocationId
				, spd.CreatedOn
			FROM [COMMON].[r365].[dbo].ServiceProvider sp
				INNER JOIN [COMMON].[r365].[dbo].ServiceProviderCustomer spc ON spc.ServiceProviderId = sp.ServiceProviderId
				INNER JOIN [COMMON].[r365].[dbo].ServiceProviderCustomerUser spcu ON spcu.CustomerName = spc.CustomerName
				INNER JOIN [COMMON].[r365].[dbo].ServiceProviderUser spu ON spu.UserId = spcu.UserId
				INNER JOIN #TmpDocuments spd ON spd.CustomerName = spcu.CustomerName AND (spd.LocationId = spcu.LocationId OR spd.LocationId = '00000000-0000-0000-0000-000000000000')
			WHERE 
				spd.[Status] = 'pending'  
				AND spc.SPEnabled = 1 
				AND sp.ServiceProviderId = COALESCE(@ServiceProviderId, sp.ServiceProviderId) 
				AND spu.EmailAddress = COALESCE(@EmailAddress, spu.EmailAddress)
				AND spu.UserLogin = COALESCE(@UserLogin, spu.UserLogin)
			GROUP BY
				  spc.CustomerName
				, spc.[Url]
				, spd.NewFileName
				, spd.LocationId
				, spd.CreatedOn
		) n
		GROUP BY 
			  CustomerName 
			, [Url]


		INSERT INTO #TmpOutput(CustomerName,[Url], _0_12, _12_16, _16_24, Over_24, Total)
		SELECT
			  UPPER(LTRIM(CustomerName)) AS 'CustomerName'
			, LTRIM([Url]) AS 'URL'
			, _0_12
			, _12_16
			, _16_24
			, Over_24
			, SUM(_0_12 + _12_16 + _16_24 + Over_24) AS Total
		FROM @dtpCount
		GROUP BY
				CustomerName
			, [Url]
			, _0_12
			, _12_16
			, _16_24
			, Over_24


		SELECT 
			CustomerName,
			[Url],
			_0_12,
			_12_16,
			_16_24,
			Over_24,
			Total
		FROM #TmpOutput
		UNION ALL
		SELECT
			  ''
			, ''
			, SUM(_0_12)
			, SUM(_12_16)
			, SUM(_16_24)
			, SUM(Over_24)
			, SUM(_0_12 + _12_16 + _16_24 + Over_24) AS Total
		FROM @dtpCount
		

		DROP TABLE #TmpOutput
		DROP TABLE #TmpDocuments
		

		SET NOCOUNT OFF;

	END TRY
	BEGIN CATCH

		SELECT  
			 ERROR_NUMBER() AS ErrorNumber  
			,ERROR_SEVERITY() AS ErrorSeverity  
			,ERROR_STATE() AS ErrorState  
			,ERROR_PROCEDURE() AS ErrorProcedure  
			,ERROR_LINE() AS ErrorLine  
			,ERROR_MESSAGE() AS ErrorMessage;  

	END CATCH

