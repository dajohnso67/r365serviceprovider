USE [ServiceProvider]
GO


--Add the ID's
IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Id'
    AND Object_ID = Object_ID('[dbo].[ServiceProviderCustomer]'))
BEGIN
    ALTER TABLE [dbo].[ServiceProviderCustomer] ADD Id INT IDENTITY;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Id'
    AND Object_ID = Object_ID('[dbo].[ServiceProviderUser]'))
BEGIN
    ALTER TABLE [dbo].[ServiceProviderUser] ADD Id INT IDENTITY;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Id'
    AND Object_ID = Object_ID('[dbo].[ServiceProviderCustomerUser]'))
BEGIN
    ALTER TABLE [dbo].[ServiceProviderCustomerUser] ADD Id INT IDENTITY;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Id'
    AND Object_ID = Object_ID('[dbo].[ServiceProviderDocument]'))
BEGIN
    ALTER TABLE [dbo].[ServiceProviderDocument] ADD Id INT IDENTITY;
END





if OBJECT_ID('[dbo].[ServiceProviderDocument]') IS NOT NULL
DROP TABLE [dbo].[ServiceProviderDocument];
GO

if OBJECT_ID('[dbo].[ServiceProviderCustomerUser]') IS NOT NULL
DROP TABLE [dbo].[ServiceProviderCustomerUser];
GO

if OBJECT_ID('[dbo].[ServiceProviderUser]') IS NOT NULL
DROP TABLE [dbo].[ServiceProviderUser];
GO

if OBJECT_ID('[dbo].[ServiceProviderCustomer]') IS NOT NULL
DROP TABLE [dbo].[ServiceProviderCustomer] ;
GO

if OBJECT_ID('[dbo].[ServiceProvider]') IS NOT NULL
DROP TABLE [dbo].[ServiceProvider] ;
GO

CREATE TABLE [dbo].[ServiceProvider] 
(
	ServiceProviderId	INT IDENTITY PRIMARY KEY, 
	ServiceProviderName VARCHAR(200) NOT NULL    

);

INSERT INTO [dbo].[ServiceProvider] (ServiceProviderName)
VALUES 
('Falcon Holdings'),
('Other')

CREATE TABLE [dbo].[ServiceProviderCustomer]      
(
	ServiceProviderId			INT,
	CustomerName				VARCHAR(200),
	[Url]						VARCHAR(320) NOT NULL,
	SPEnabled					BIT NOT NULL,

);

CREATE TABLE [dbo].[ServiceProviderUser]
(
	CustomerName	VARCHAR(200),
    UserId			UNIQUEIDENTIFIER NOT NULL,
    FullName		VARCHAR(100),
	EmailAddress	VARCHAR(320),
	UserLogin		VARCHAR(50),
	[Password]		VARCHAR(50),

);

CREATE TABLE [dbo].[ServiceProviderCustomerUser]
(
    
    CustomerName	VARCHAR(200),
	LocationId		UNIQUEIDENTIFIER,
	UserId			UNIQUEIDENTIFIER,

);

CREATE TABLE [dbo].[ServiceProviderDocument]
(

	FileId			INT,
    CustomerName	VARCHAR(200),
    LocationId		UNIQUEIDENTIFIER,
	UserId			UNIQUEIDENTIFIER,
	AssignedTo		UNIQUEIDENTIFIER,
	[Status]		VARCHAR(50),
	NewFileName		VARCHAR(200),
	CreatedOn		DATETIME,
	ModifiedOn		DATETIME,
	ModifiedBy		UNIQUEIDENTIFIER,

);



ALTER TABLE [dbo].[ServiceProviderUser] ADD CustomerName VARCHAR(200);
ALTER TABLE [dbo].[ServiceProviderDocument] ADD UserId UNIQUEIDENTIFIER;
ALTER TABLE [dbo].[ServiceProviderDocument] ALTER COLUMN ModifiedBy UNIQUEIDENTIFIER;
