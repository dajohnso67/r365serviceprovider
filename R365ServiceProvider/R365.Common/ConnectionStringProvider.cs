﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace R365.Common
{
    public class ConnectionString
    {
        public ConnectionString() { }
        public string DataSource { get; set; }
        public string InitialCatalog { get; set; }
        public string IntegratedSecurity { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public string MultipleActiveResultSets { get; set; }
        public EnvironmentType EnvironmentType { get; set; }
    }

    public class EnvironmentTypes
    {
        public EnvironmentType EnvironmentType { get; set; }
    }


    public class KnownTypeProvider : List<Type>
    {
        public KnownTypeProvider() { }

    }


    public class EnvironmentProvider : List<EnvironmentTypes> {
        public EnvironmentProvider() { }

    }

    public class ConnectionStringProvider : List<ConnectionString>
    {
        public string ConnectionString { get; set; }

        public ConnectionStringProvider() { }

        public void SetConnectionString(EnvironmentType evType)
        {
            var slConn = this.Where(c => c.EnvironmentType == evType).Single();

            string connstring = $"" +
                $"Data Source={slConn.DataSource};" +
                $"Initial Catalog={slConn.InitialCatalog};" +
                $"Integrated Security={slConn.IntegratedSecurity};" +
                $"User Id={slConn.UserId};" +
                $"Password={slConn.Password};" +
                $"MultipleActiveResultSets={slConn.MultipleActiveResultSets}";

            ConnectionString = connstring;
        }
    }

    public static class ConnectionStringExtensions
    {
        public static string GetEnvironmentConnectionString(this ConnectionStringProvider conn, EnvironmentType evType)
        {
            var slConn = conn.Where(c => c.EnvironmentType == evType).Single();

            string ConnectionString = $"" +
                $"Data Source={slConn.DataSource};" +
                $"Initial Catalog={slConn.InitialCatalog};" +
                $"Integrated Security={slConn.IntegratedSecurity};" +
                $"User Id={slConn.UserId};" +
                $"Password={slConn.Password};" +
                $"MultipleActiveResultSets={slConn.MultipleActiveResultSets}";

            return ConnectionString;

        }
    }
}
