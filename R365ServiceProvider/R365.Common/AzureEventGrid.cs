﻿
namespace R365.Common
{
    public class AzureEventGrid
    {
        public string SubscriptionId { get; set; }
        public string EventName { get; set; }
        public string ValidationEventType { get; set; }
    }
}
