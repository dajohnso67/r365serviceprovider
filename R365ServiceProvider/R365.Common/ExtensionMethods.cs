﻿using System;

namespace R365.Common
{
    public static class ExtensionMethods
    {
        public static bool ContainsIgnoreCase(this string source, string value) {
            return source.Contains(value, StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool EqualsIgnoreCase(this string source, string value){
            return source.Equals(value, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
