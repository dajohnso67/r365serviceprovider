﻿using System;

namespace R365.Common
{
    public static class Enums
    {
        public static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
    }

    public enum EnvironmentType
    {
        Azure,
        SoftLayer
    };

    public enum EnvelopeType
    {
        contract,
        catalogue
    };

    public enum ServiceProviderDocumentStatus
    {
        Pending,
        Complete
    };
}
