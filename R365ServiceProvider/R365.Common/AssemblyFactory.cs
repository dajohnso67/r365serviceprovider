﻿using System;
using System.Reflection;

namespace R365.Common
{
    public static class AssemblyFactory
    {
        public static Type[] GetAssemblyTypes(string assembyType)
        {
            return Assembly.Load(new System.Reflection.AssemblyName(assembyType)).GetTypes();
        }
    }
}
