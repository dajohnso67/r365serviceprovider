﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;

namespace R365.Common
{
    public class JsonContent : StringContent
    {
        public JsonContent(object obj) : base(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json") { }
    }

    public class JsonHelper
    {

        /// <summary>
        /// JSON Deserialization
        /// </summary>
        public static object JsonDeserialize(string jsonString, Type T, List<Type> knownTypes)
        {
            object Envelope = null;

            DataContractJsonSerializerSettings settings = new DataContractJsonSerializerSettings
            {
                DateTimeFormat = new System.Runtime.Serialization.DateTimeFormat("yyyy-MM-dd'T'HH:mm:ssZ"),
                KnownTypes = knownTypes
            };

            using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString)))
            {
                DataContractJsonSerializer ser = new DataContractJsonSerializer(T, settings);
                Envelope = ser.ReadObject(ms);
            }

            return Envelope;
        }


        /// <summary>
        /// JSON Deserialization
        /// </summary>
        public static object JsonDeserialize(string jsonString, Type T)
        {
            object Envelope = null;

            DataContractJsonSerializerSettings settings = new DataContractJsonSerializerSettings
            {
                DateTimeFormat = new System.Runtime.Serialization.DateTimeFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            };

            using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString)))
            {
                DataContractJsonSerializer ser = new DataContractJsonSerializer(T, settings);
                Envelope = ser.ReadObject(ms);
            }

            return Envelope;
        }

        /// <summary>
        /// JSON Deserialization
        /// </summary>
        public static object JsonDeserialize<T>(string jsonString)
        {
            object Envelope = null;

            DataContractJsonSerializerSettings settings = new DataContractJsonSerializerSettings
            {
                DateTimeFormat = new System.Runtime.Serialization.DateTimeFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            };

            using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString)))
            {
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T), settings);
                Envelope = ser.ReadObject(ms);
            }

            return Envelope;
        }
    }
}
