﻿using Autofac;
using MediatR;
using R365.Infrastructure.Data;
using R365.Infrastructure.Http;
using System;
using System.Linq;
using R365.Common;
using R365.Commands;
using Microsoft.AspNetCore.Http;
using LazyCache;

namespace R365.API.DependencyInjection
{
    public class ServiceRegistry : Module
    {
        private DbExecute DbExec { get; set; }
        private ConnectionStringProvider _DbConnect { get; set; }
        private EnvironmentProvider _environments { get; set; }
        private IAppCache _appCache { get; set; }
        private AzureEventGrid _azEventGrid { get; set; }

        public ServiceRegistry(ConnectionStringProvider DbConnect, AzureEventGrid AzEventGrid, EnvironmentProvider environments) {

            _DbConnect = DbConnect;
            DbExec = new DbExecute(DbConnect);
            _appCache = new CachingService();
            _azEventGrid = AzEventGrid;
            _environments = environments;

        }

        /// <summary>
        /// Load
        /// </summary>
        /// <param name="builder"></param>
        protected override void Load(ContainerBuilder builder) {

            builder.RegisterType<HttpContextAccessor>().As<IHttpContextAccessor>().SingleInstance();
            builder.RegisterInstance(DbExec).As<DbExecute>();

            builder.RegisterInstance(_azEventGrid).As<AzureEventGrid>();

            builder.RegisterInstance(_appCache).As<IAppCache>().SingleInstance();

            builder.RegisterInstance(_DbConnect).As<ConnectionStringProvider>();

            builder.RegisterInstance(_environments).As<EnvironmentProvider>();

            builder.RegisterType<DataContext>().AsSelf().InstancePerLifetimeScope();
            builder.RegisterType<ClientConnectionFactory>().As<IClientConnectionFactory>().SingleInstance();

            Type[] _IRequestHandler = AssemblyFactory.GetAssemblyTypes("R365.API").Where(t => t.IsClosedTypeOf(typeof(IRequestHandler<,>))).ToArray<Type>();
            Type[] _IRequest1 = AssemblyFactory.GetAssemblyTypes("R365.API").Where(t => t.IsClosedTypeOf(typeof(IRequest<>))).ToArray<Type>();
            Type[] _IRequest2 = AssemblyFactory.GetAssemblyTypes("R365.API").Where(t => t.IsAssignableFrom(typeof(IRequest))).ToArray<Type>();

            foreach (Type _tp in _IRequestHandler)
            {
                builder.RegisterAssemblyTypes(_tp.Assembly).AsClosedTypesOf(typeof(IRequestHandler<,>));
            }

            foreach (Type _tp in _IRequest1)
            {
                builder.RegisterAssemblyTypes(_tp.Assembly).AsClosedTypesOf(typeof(IRequest<>));
            }

            foreach (Type _tp in _IRequest2)
            {
                builder.RegisterAssemblyTypes(_tp.Assembly).AsClosedTypesOf(typeof(IRequest));
            }

        }
    }
}
