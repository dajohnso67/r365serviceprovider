﻿using R365.Domain;
using System;
using System.Collections.Generic;

namespace R365.API.Commands.SPCustomerUser
{

    public class ServiceProviderCustomerUserEnvelope : ServiceProviderBase
    {
        public List<SPCU_Contract> Contract { get; set; }
        public ServiceProviderCustomerUserEnvelope() => Contract = new List<SPCU_Contract>();
    }


    public class SPCU_Contract
    {
        public Guid LocationId { get; set; }
        public Guid UserId { get; set; }
    }
}
