﻿using EFCore.BulkExtensions;
using MediatR;
using R365.Commands;
using R365.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace R365.API.Commands.SPCustomerUser
{
    public class Edit
    {
        public class UpdateServiceProviderCustomerUserCommand : IRequest
        {
            public ServiceProviderCustomerUserEnvelope Envelope { get; set; }
        }

        public class Handler : IRequestHandler<UpdateServiceProviderCustomerUserCommand>
        {

            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }


            Task<Unit> IRequestHandler<UpdateServiceProviderCustomerUserCommand, Unit>.Handle(UpdateServiceProviderCustomerUserCommand request, CancellationToken cancellationToken)
            {

                foreach (var User in request.Envelope.Contract)
                {

                    var items = _context.ServiceProviderCustomerUser.Where(c => c.CustomerName == request.Envelope.Catalogue.CustomerName && c.UserId == User.UserId).ToList();

                    _context.ServiceProviderCustomerUser.RemoveRange(items);
                    _context.SaveChanges(true);

                }

                List<ServiceProviderCustomerUser> spcu = new List<ServiceProviderCustomerUser>();

                foreach (var item in request.Envelope.Contract)
                {
                    spcu.Add(new ServiceProviderCustomerUser()
                    {
                        LocationId = item.LocationId,
                        UserId = item.UserId,
                        CustomerName = request.Envelope.Catalogue.CustomerName
                    });
                }

                _context.BulkInsert(spcu);
                _context.SaveChanges(true);
                _context.Dispose();

                return Unit.Task;
            }
        }

    }
}
