﻿using R365.Common;
using R365.Domain;
using System;
using System.Collections.Generic;

namespace R365.API.Commands.SPDocument
{

    public class ServiceProviderDocumentEnvelope : ServiceProviderBase
    {
        public List<SPD_Contract> Contract { get; set; }
        public ServiceProviderDocumentEnvelope() => Contract = new List<SPD_Contract>();

    }

    public class SPD_Contract
    {
        public int? FileId { get; set; }
        public Guid? LocationId { get; set; }
        public Guid? AssignedTo { get; set; }
        public ServiceProviderDocumentStatus Status { get; set; }
        public string NewFileName { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        public Guid? UserId { get; set; }
    }

}
