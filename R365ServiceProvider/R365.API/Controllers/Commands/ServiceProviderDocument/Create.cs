﻿using EFCore.BulkExtensions;
using MediatR;
using R365.Commands;
using R365.Common;
using R365.Domain;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace R365.API.Commands.SPDocument
{
    public class Create
    {
        public class CreateServiceProviderDocumentCommand : IRequest
        {
            public ServiceProviderDocumentEnvelope Envelope { get; set; }
        }

        public class Handler : IRequestHandler<CreateServiceProviderDocumentCommand>
        {

            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            /// <summary>
            /// IRequestHandler<ServiceProviderDocumentCreateCommand, Unit>.Handle
            /// </summary>
            /// <param name="request"></param>
            /// <param name="cancellationToken"></param>
            /// <returns></returns>
            Task<Unit> IRequestHandler<CreateServiceProviderDocumentCommand, Unit>.Handle(CreateServiceProviderDocumentCommand request, CancellationToken cancellationToken)
            {
                List<ServiceProviderDocument> spcu = new List<ServiceProviderDocument>();

                foreach (var document in request.Envelope.Contract)
                {
                    spcu.Add(

                        new ServiceProviderDocument()
                        {
                            Id = null,
                            AssignedTo = document.AssignedTo,
                            CreatedOn = (document.CreatedOn == null) ? DateTime.Now : document.CreatedOn,
                            CustomerName = request.Envelope.Catalogue.CustomerName,
                            FileId = document.FileId,
                            LocationId = (document.LocationId == null) ? Guid.Parse("00000000-0000-0000-0000-000000000000") : document.LocationId,
                            ModifiedBy = document.ModifiedBy,
                            ModifiedOn = document.ModifiedOn,
                            NewFileName = document.NewFileName,
                            Status = ServiceProviderDocumentStatus.Pending.ToString(),
                            UserId = document.UserId
                        }
                    );
                }

                _context.BulkInsert(spcu);
                _context.SaveChanges(true);
                _context.Dispose();

                return Unit.Task;
            }
        }

    }
}
