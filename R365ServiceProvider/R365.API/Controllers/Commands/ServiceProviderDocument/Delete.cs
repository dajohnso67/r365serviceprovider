﻿using MediatR;
using R365.Commands;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace R365.API.Commands.SPDocument
{
    public class Delete
    {
        public class RemoveServiceProviderDocumentCommand : IRequest
        {
            public ServiceProviderDocumentEnvelope Envelope { get; set; }
        }

        public class Handler : IRequestHandler<RemoveServiceProviderDocumentCommand>
        {

            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            /// <summary>
            /// IRequestHandler<ServiceProviderDocumentEditCommand, Unit>.Handle
            /// </summary>
            /// <param name="request"></param>
            /// <param name="cancellationToken"></param>
            /// <returns></returns>
            Task<Unit> IRequestHandler<RemoveServiceProviderDocumentCommand, Unit>.Handle(RemoveServiceProviderDocumentCommand request, CancellationToken cancellationToken)
            {

                foreach(var document in request.Envelope.Contract)
                {
                    var entity = _context.ServiceProviderDocument.Where(item =>
                        item.FileId == document.FileId &&
                        item.CustomerName == request.Envelope.Catalogue.CustomerName
                    ).SingleOrDefault();

                    _context.ServiceProviderDocument.Remove(entity);
                }

                _context.SaveChanges(true);
                _context.Dispose();

                return Unit.Task;
            }
        }

    }
}
