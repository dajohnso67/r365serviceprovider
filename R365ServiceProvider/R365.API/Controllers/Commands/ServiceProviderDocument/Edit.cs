﻿using MediatR;
using R365.Commands;
using R365.Domain;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace R365.API.Commands.SPDocument
{
    public class Edit
    {
        public class UpdateServiceProviderDocumentCommand : IRequest
        {
            public ServiceProviderDocumentEnvelope Envelope { get; set; }
        }

        public class Handler : IRequestHandler<UpdateServiceProviderDocumentCommand>
        {

            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            /// <summary>
            /// IRequestHandler<ServiceProviderDocumentEditCommand, Unit>.Handle
            /// </summary>
            /// <param name="request"></param>
            /// <param name="cancellationToken"></param>
            /// <returns></returns>
            Task<Unit> IRequestHandler<UpdateServiceProviderDocumentCommand, Unit>.Handle(UpdateServiceProviderDocumentCommand request, CancellationToken cancellationToken)
            {

                foreach (var document in request.Envelope.Contract) {

                    var entity = _context.ServiceProviderDocument.Where(item =>
                        item.FileId == document.FileId &&
                        item.CustomerName == request.Envelope.Catalogue.CustomerName
                    ).SingleOrDefault();

                    entity.ModifiedOn = DateTime.Now;
                    entity.Status = (!string.Equals(document.Status.ToString(), entity.Status.ToString(), StringComparison.OrdinalIgnoreCase)) ? document.Status.ToString() : entity.Status.ToString();

                    if (!object.Equals(document.LocationId, null)) {
                        entity.LocationId = document.LocationId;
                    }

                    if (!object.Equals(document.AssignedTo, null)) {
                        entity.AssignedTo = document.AssignedTo;
                    }

                    if (!object.Equals(document.CreatedOn, null)) {
                        entity.CreatedOn = document.CreatedOn;
                    }

                    if (!object.Equals(document.ModifiedBy, null)) {
                        entity.ModifiedBy = document.ModifiedBy;
                    }

                    if (!string.IsNullOrEmpty(document.NewFileName)) {
                        entity.NewFileName = document.NewFileName;
                    }

                    if (!object.Equals(document.UserId, null)) {
                        entity.UserId = document.UserId;
                    }

                    _context.ServiceProviderDocument.Update(entity);
                }

                _context.SaveChanges(true);
                _context.Dispose();

                return Unit.Task;
            }
        }
    }
}
