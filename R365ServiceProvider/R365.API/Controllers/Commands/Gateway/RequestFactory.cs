﻿using Microsoft.Azure.EventGrid.Models;
using System;
using System.Reflection;
using R365.Common;
using System.Linq;
using R365.Domain;
using Microsoft.Extensions.DependencyInjection;

namespace R365.API.Gateway
{
    /// <summary>
    /// EventGridEventFactoryExtension
    /// </summary>
    public static class EventGridEventFactoryExtension
    {

        private static readonly IServiceProvider Provider;

        static EventGridEventFactoryExtension() {

            KnownTypeProvider knownTypes = new KnownTypeProvider();

            knownTypes.AddRange(AssemblyFactory.GetAssemblyTypes("R365.API")
              .Where(t => t.BaseType == (typeof(ServiceProviderBase)))
              .Select(v => v.GetProperties()[0].PropertyType).ToList<Type>());

            Provider = new ServiceCollection().AddSingleton<KnownTypeProvider>(knownTypes).BuildServiceProvider();

        }

        /// <summary>
        /// CreateIRequest
        /// </summary>
        /// <param name="eventGridEvent"></param>
        /// <returns></returns>
        public static T CreateRequest<T>(this EventGridEvent eventGridEvent)
        {
            Assembly currentAssem = Assembly.GetExecutingAssembly();

            string newAssembName = eventGridEvent.EventType
                .Replace(".Create.", ".Create+", StringComparison.InvariantCultureIgnoreCase)
                .Replace(".Edit.", ".Edit+", StringComparison.InvariantCultureIgnoreCase)
                .Replace(".Delete.", ".Delete+", StringComparison.InvariantCultureIgnoreCase);

            Type objectType = currentAssem.GetType(newAssembName, true, true);

            object obj = Activator.CreateInstance(objectType);

            PropertyInfo prop = obj.GetType().GetProperty("Envelope", BindingFlags.Public | BindingFlags.Instance);

            Type envType = obj.GetType().GetProperties()[0].PropertyType;

            KnownTypeProvider knownTypes = Provider.GetService<KnownTypeProvider>();

            if (null != prop && prop.CanWrite)
            {
                prop.SetValue(obj, JsonHelper.JsonDeserialize(eventGridEvent.Data.ToString(), envType, knownTypes), null);
            }

            return (T)obj;

        }


        /// <summary>
        /// CreateTest<T>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="eventGridEvent"></param>
        /// <returns></returns>
        public static T CreateTestRequest<T>(this EventGridEvent eventGridEvent)
        {
            Assembly currentAssem = Assembly.GetExecutingAssembly();

            string newAssembName = eventGridEvent.EventType
                .Replace(".Create.", ".Create+", StringComparison.InvariantCultureIgnoreCase)
                .Replace(".Edit.", ".Edit+", StringComparison.InvariantCultureIgnoreCase)
                .Replace(".Delete.", ".Delete+", StringComparison.InvariantCultureIgnoreCase);

            Type objectType = currentAssem.GetType(newAssembName, true, true);

            object obj = Activator.CreateInstance(objectType);

            PropertyInfo prop = obj.GetType().GetProperty("Envelope", BindingFlags.Public | BindingFlags.Instance);

            if (null != prop && prop.CanWrite)
            {
                prop.SetValue(obj, eventGridEvent.Data, null);
            }

            return (T)obj;
        }
    }
}
