﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.EventGrid.Models;
using System.Threading.Tasks;

namespace R365.API.Gateway
{
    [Route("api/[controller]")]
    public class GatewayController : Controller
    {
        private readonly IMediator _mediator;

        public GatewayController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Run
        /// -----------------------------------------
        /// -----------------------------------------
        /// CONTRACT TYPES
        /// -----------------------------------------
        /// -----------------------------------------
        /// SERVICEPROVIDER
        /// 
        /// Create.CreateServiceProviderCommand
        /// Edit.UpdateServiceProvidertCommand
        /// 
        /// -----------------------------------------
        /// SERVIEPROVIDERCUSTOMER
        /// 
        /// Create.CreateServiceProviderCustomerCommand
        /// Edit.UpdateServiceProviderCustomerCommand
        /// 
        /// -----------------------------------------
        /// SERVICEPROVIDERCUSTOMERUSER
        /// 
        /// Create.CreateServiceProviderCustomerUserCommand
        /// Edit.UpdateServiceProviderCustomerUserCommand
        /// 
        /// -----------------------------------------
        /// SERVICEPROVIDERDOCUMENT
        /// 
        /// Create.CreateServiceProviderDocumentCommand
        /// Edit.UpdateServiceProviderDocumentCommand
        /// Delete.RemoveServiceProviderDocumentCommand
        /// 
        /// -----------------------------------------
        /// SERVICEPROVIDERUSER
        /// 
        /// Create.CreateServiceProviderUserCommand
        /// Edit.UpdateServiceProviderUserCommand
        /// 
        /// </summary>
        /// <param type="EventGridEvent[]"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("[action]")]
        [HttpPost]
        public async Task<IActionResult> Run([FromBody]EventGridEvent[] request)
        {
            foreach (EventGridEvent eventGridEvent in request)
            {
                await _mediator.Send(eventGridEvent.CreateRequest<IRequest>());
            }

            return Ok();
        }
    }
}