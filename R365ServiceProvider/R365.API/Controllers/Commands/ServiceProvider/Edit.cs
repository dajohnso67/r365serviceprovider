﻿using MediatR;
using R365.Commands;
using R365.Domain;
using System.Threading;
using System.Threading.Tasks;

namespace R365.API.Commands.SP
{
    public class Edit
    {
        public class UpdateServiceProvidertCommand : IRequest
        {
            public ServiceProviderEnvelope Envelope { get; set; }
        }

        public class Handler : IRequestHandler<UpdateServiceProvidertCommand>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            /// <summary>
            /// IRequestHandler<ServiceProviderEditCommand, Unit>.Handle
            /// </summary>
            /// <param name="request"></param>
            /// <param name="cancellationToken"></param>
            /// <returns></returns>
            Task<Unit> IRequestHandler<UpdateServiceProvidertCommand, Unit>.Handle(UpdateServiceProvidertCommand request, CancellationToken cancellationToken)
            {

                ServiceProvider sp = new ServiceProvider()
                {
                    ServiceProviderId = request.Envelope.Contract.ServiceProviderId,
                    ServiceProviderName = request.Envelope.Contract.ServiceProviderName
                };

                _context.Update<ServiceProvider>(sp);
                _context.SaveChanges(true);
                _context.Dispose();

                return Unit.Task;
            }
        }
    }
}
