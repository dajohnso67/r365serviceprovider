﻿using R365.Domain;

namespace R365.API.Commands.SP
{

    public class ServiceProviderEnvelope : ServiceProviderBase
    {
        public SP_Contract Contract { get; set; }
    }


    public class SP_Contract
    {
        public int? ServiceProviderId { get; set; }
        public string ServiceProviderName { get; set; }
    }
}
