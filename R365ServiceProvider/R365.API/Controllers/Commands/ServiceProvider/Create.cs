﻿using MediatR;
using R365.Commands;
using R365.Domain;
using System.Threading;
using System.Threading.Tasks;

namespace R365.API.Commands.SP
{
    public class Create
    {
        public class CreateServiceProviderCommand : IRequest
        {
            public ServiceProviderEnvelope Envelope { get; set; }
        }

        public class Handler : IRequestHandler<CreateServiceProviderCommand>
        {

            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            /// <summary>
            /// IRequestHandler<ServiceProviderCreateCommand, Unit>.Handle
            /// </summary>
            /// <param name="request"></param>
            /// <param name="cancellationToken"></param>
            /// <returns></returns>
            Task<Unit> IRequestHandler<CreateServiceProviderCommand, Unit>.Handle(CreateServiceProviderCommand request, CancellationToken cancellationToken)
            {
                request.Envelope.Contract.ServiceProviderId = null;

                ServiceProvider sp = new ServiceProvider() {
                     ServiceProviderId = request.Envelope.Contract.ServiceProviderId,
                     ServiceProviderName = request.Envelope.Contract.ServiceProviderName
                };

                _context.AddAsync<ServiceProvider>(sp);
                _context.SaveChanges(true);
                _context.Dispose();

                return Unit.Task;
            }
        }
    }
}
