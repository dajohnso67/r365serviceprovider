﻿using R365.Domain;

namespace R365.API.Commands.SPCustomer
{

    public class ServiceProviderCustomerEnvelope : ServiceProviderBase
    {
        public SPC_Contract Contract { get; set; }
    }

    public class SPC_Contract {

        public int? ServiceProviderId { get; set; }
        public bool? SPEnabled { get; set; }
    }
}
