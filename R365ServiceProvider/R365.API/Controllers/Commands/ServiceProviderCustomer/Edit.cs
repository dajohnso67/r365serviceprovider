﻿using MediatR;
using R365.Commands;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace R365.API.Commands.SPCustomer
{
    public class Edit
    {
        public class UpdateServiceProviderCustomerCommand : IRequest
        {
            public ServiceProviderCustomerEnvelope Envelope { get; set; }
        }

        public class Handler : IRequestHandler<UpdateServiceProviderCustomerCommand>
        {

            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            /// <summary>
            /// IRequestHandler<ServiceProviderCustomerEditCommand, Unit>.Handle
            /// </summary>
            /// <param name="request"></param>
            /// <param name="cancellationToken"></param>
            /// <returns></returns>
            Task<Unit> IRequestHandler<UpdateServiceProviderCustomerCommand, Unit>.Handle(UpdateServiceProviderCustomerCommand request, CancellationToken cancellationToken)
            {

                var entity = _context.ServiceProviderCustomer.FirstOrDefault(item => item.CustomerName == request.Envelope.Catalogue.CustomerName);

                bool? spEnabled = request.Envelope.Contract.SPEnabled;
                int? serviceProviderId = request.Envelope.Contract.ServiceProviderId;

                entity.SPEnabled = (spEnabled == null) ? entity.SPEnabled : spEnabled;

                entity.ServiceProviderId = (serviceProviderId == null) ? entity.ServiceProviderId : serviceProviderId;

                if (spEnabled != null || serviceProviderId != null) {

                    _context.ServiceProviderCustomer.Update(entity);
                    _context.SaveChanges(true);
                    _context.Dispose();
                }

                return Unit.Task;
            }
        }
    }
}
