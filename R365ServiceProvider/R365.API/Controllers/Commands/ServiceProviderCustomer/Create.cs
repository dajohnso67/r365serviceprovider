﻿using MediatR;
using Microsoft.AspNetCore.Http;
using R365.Commands;
using R365.Domain;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace R365.API.Commands.SPCustomer
{
    public class Create
    {
        public class CreateServiceProviderCustomerCommand : IRequest
        {
            public ServiceProviderCustomerEnvelope Envelope { get; set; }
        }

        public class Handler : IRequestHandler<CreateServiceProviderCustomerCommand>
        {

            private readonly DataContext _context;
            private readonly IHttpContextAccessor _accessor;

            public Handler(DataContext context, IHttpContextAccessor accessor)
            {
                _accessor = accessor;
                _context = context;
            }

            /// <summary>
            /// IRequestHandler<ServiceProviderCustomerCreateCommand, Unit>.Handle
            /// </summary>
            /// <param name="request"></param>
            /// <param name="cancellationToken"></param>
            /// <returns></returns>
            Task<Unit> IRequestHandler<CreateServiceProviderCustomerCommand, Unit>.Handle(CreateServiceProviderCustomerCommand request, CancellationToken cancellationToken)
            {

                var items = _context.ServiceProviderCustomer.Where(c => c.CustomerName == request.Envelope.Catalogue.CustomerName).ToList();

                if (!items.Equals(null)){
                    _context.ServiceProviderCustomer.RemoveRange(items);
                    _context.SaveChanges(true);
                }


                ServiceProviderCustomer cust = new ServiceProviderCustomer()
                {
                    CustomerName = request.Envelope.Catalogue.CustomerName,
                    ServiceProviderId = request.Envelope.Contract.ServiceProviderId,
                    SPEnabled = request.Envelope.Contract.SPEnabled,
                    Url = "https://" + request.Envelope.Catalogue.CustomerName + ".restaurant365." + _accessor.HttpContext.Items["url_Type"].ToString(),
                    Id = null
                };

                _context.AddAsync<ServiceProviderCustomer>(cust);
                _context.SaveChanges(true);
                _context.Dispose();


                return Unit.Task;
            }
        }
    }
}
