﻿using MediatR;
using R365.Commands;
using R365.Domain;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace R365.API.Commands.SPUser
{
    public class Create
    {
        public class CreateServiceProviderUserCommand : IRequest
        {
            public ServiceProviderUserEnvelope Envelope { get; set; }
        }

        public class Handler : IRequestHandler<CreateServiceProviderUserCommand>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            /// <summary>
            /// IRequestHandler<ServiceProviderUserCreateCommand, Unit>.Handle
            /// </summary>
            /// <param name="request"></param>
            /// <param name="cancellationToken"></param>
            /// <returns></returns>
            Task<Unit> IRequestHandler<CreateServiceProviderUserCommand, Unit>.Handle(CreateServiceProviderUserCommand request, CancellationToken cancellationToken)
            {

                var items = _context.ServiceProviderUser.Where(c => c.CustomerName == request.Envelope.Catalogue.CustomerName && c.UserId == request.Envelope.Contract.UserId).ToList();

                if (!items.Equals(null))
                {
                    _context.ServiceProviderUser.RemoveRange(items);
                    _context.SaveChanges(true);
                }

                _context.AddAsync<ServiceProviderUser>(

                    new ServiceProviderUser()
                    {
                        CustomerName = request.Envelope.Catalogue.CustomerName,
                        EmailAddress = request.Envelope.Contract.EmailAddress,
                        FullName = request.Envelope.Contract.FullName,
                        UserId = request.Envelope.Contract.UserId,
                        UserLogin = request.Envelope.Contract.UserLogin
                    });

                _context.SaveChanges(true);
                _context.Dispose();

                return Unit.Task;
            }
        }
    }
}
