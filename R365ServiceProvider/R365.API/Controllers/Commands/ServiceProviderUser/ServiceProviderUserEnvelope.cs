﻿using R365.Domain;
using System;

namespace R365.API.Commands.SPUser
{

    public class ServiceProviderUserEnvelope : ServiceProviderBase
    {
        public SPU_Contract Contract { get; set; }
    }

    public class SPU_Contract
    {
        public Guid UserId { get; set; }
        public string FullName { get; set; }
        public string EmailAddress { get; set; }
        public string UserLogin { get; set; }
    }
}
