﻿using MediatR;
using R365.Commands;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace R365.API.Commands.SPUser
{
    public class Edit
    {
        public class UpdateServiceProviderUserCommand : IRequest
        {
            public ServiceProviderUserEnvelope Envelope { get; set; }
        }

        public class Handler : IRequestHandler<UpdateServiceProviderUserCommand>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            /// <summary>
            /// IRequestHandler<ServiceProviderUserEditCommand, Unit>.Handle
            /// </summary>
            /// <param name="request"></param>
            /// <param name="cancellationToken"></param>
            /// <returns></returns>
            Task<Unit> IRequestHandler<UpdateServiceProviderUserCommand, Unit>.Handle(UpdateServiceProviderUserCommand request, CancellationToken cancellationToken)
            {
                foreach (var r in _context.ServiceProviderUser.Where(item => item.UserId == request.Envelope.Contract.UserId && item.CustomerName == request.Envelope.Catalogue.CustomerName))
                {
                    r.CustomerName = request.Envelope.Catalogue.CustomerName;
                    r.EmailAddress = request.Envelope.Contract.EmailAddress;
                    r.FullName = request.Envelope.Contract.FullName;
                    r.UserLogin = request.Envelope.Contract.UserLogin;

                    _context.ServiceProviderUser.Update(r);
                }

                _context.SaveChanges(true);
                _context.Dispose();


                return Unit.Task;
            }
        }
    }
}
