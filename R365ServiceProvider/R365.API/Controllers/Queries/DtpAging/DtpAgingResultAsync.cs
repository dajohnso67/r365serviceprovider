﻿namespace R365.API.DtpAging
{
    public class DtpAgingResult
    {
        public string CustomerName { get; set; }
        public string Url { get; set; }
        public int _0_12 { get; set; }
        public int _12_16 { get; set; }
        public int _16_24 { get; set; }
        public int Over_24 { get; set; }
        public int Total { get; set; }
    }
}
