﻿using MediatR;
using System;
using System.Collections.Generic;

namespace R365.API.DtpAging
{
    public class DtpAgingQueryAsync : IRequest<List<DtpAgingResult>>
    {
        public int?     ServiceProviderId { get; set; }
        public Guid?    UserId { get; set; }
        public string   EmailAddress { get; set; }
        public string   UserLogin { get; set; }
        public bool?    ShowUnassigned { get; set; }
    }
}
