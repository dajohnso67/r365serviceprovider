﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using R365.API.DtpAging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace R365.API.Controllers.DtpAging
{
    [Route("api/[controller]")]
    public class DtpAgingController : Controller
    {
        private readonly IMediator _mediator;

        public DtpAgingController(IMediator mediator)
        {
            this._mediator = mediator;
        }

        /// <summary>
        /// Post
        /// </summary>
        /// <param type="DtpAgingQueryAsync"></param>
        /// <param name="aging"></param>
        /// <returns></returns>
        [Route("[action]")]
        [Produces(typeof(IEnumerable<DtpAgingResult>))]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]DtpAgingQueryAsync aging)
        {
            var result = await _mediator.Send(aging);
            return new ObjectResult(result);
        }
    }
}