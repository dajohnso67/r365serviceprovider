﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using R365.Infrastructure.Data;
using System.Data;
using System.Collections.Generic;
using R365.Common;
using System.Linq;
using Microsoft.AspNetCore.Hosting;

namespace R365.API.DtpAging
{
    public class DtpAgingQueryHandlerAsync : IRequestHandler<DtpAgingQueryAsync, List<DtpAgingResult>>
    {

        private readonly DbExecute _exec;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly EnvironmentProvider _environments;

        public DtpAgingQueryHandlerAsync(DbExecute exec, IHostingEnvironment hostingEnvironment, EnvironmentProvider environments) {

            _exec = exec;
            _hostingEnvironment = hostingEnvironment;
            _environments = environments;
        }

        /// <summary>
        /// Handle
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<List<DtpAgingResult>> Handle(DtpAgingQueryAsync request, CancellationToken cancellationToken)
        {
            SqlParamCollection p = new SqlParamCollection
            {
                { "@ServiceProviderId", request?.ServiceProviderId, DbType.Int32, ParameterDirection.Input, null },
                { "@EmailAddress", request?.EmailAddress, DbType.String, ParameterDirection.Input, 320 },
                { "@UserLogin", request?.UserLogin, DbType.String, ParameterDirection.Input, 200 },
                { "@UserId", request?.UserId, DbType.Guid, ParameterDirection.Input, null },
                { "@ShowUnassigned", request?.ShowUnassigned, DbType.Boolean, ParameterDirection.Input, null }
            };

            List<DtpAgingResult> result = new List<DtpAgingResult>();

            foreach (var envItem in _environments) {

                _exec.EnvironmentType = envItem.EnvironmentType;
                result.AddRange(await _exec.QueryAsync<DtpAgingResult>(SProc.DTP_Aging_Report, p));
            }

            result = result.Where(c => !string.IsNullOrEmpty(c.CustomerName)).OrderByDescending(o => o.Total).ToList();

            DtpAgingResult agList = new DtpAgingResult()
            {

                _0_12 = result.Sum(c => c._0_12),
                _12_16 = result.Sum(c => c._12_16),
                _16_24 = result.Sum(c => c._16_24),
                Over_24 = result.Sum(c => c.Over_24),
                Total = result.Sum(c => c.Total)
            };

            result.Insert(result.Count(), agList);

            return result;
        }

    }

}
