﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using R365.API.DependencyInjection;
using R365.API.MiddleWare;
using R365.API.MiddleWare.Filters;
using R365.Common;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using Serilog.Exceptions;
using Serilog.Exceptions.Core;
using Serilog.Exceptions.Destructurers;
using Serilog.Exceptions.SqlServer.Destructurers;
using Serilog.Sinks.Elasticsearch;
using System;
using System.Collections.Generic;


namespace R365.API
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IContainer ApplicationContainer { get; private set; }
        private ConnectionStringProvider DbConnect { get; set; }
        private EnvironmentProvider EnvTypes { get; set; }

        private AzureEventGrid AzEventGrid { get; set; }
        /// <summary>
        /// Startup
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="hostingEnvironment"></param>
        public Startup(IConfiguration configuration, IHostingEnvironment hostingEnvironment)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(hostingEnvironment.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{hostingEnvironment.EnvironmentName}.json", reloadOnChange: true, optional: true)
                .AddEnvironmentVariables()
                .AddApplicationInsightsSettings();


            Configuration = builder.Build();
            DbConnect = Configuration.GetSection("Data:ConnectionStrings").Get<ConnectionStringProvider>();
            EnvTypes = Configuration.GetSection("Data:ConnectionStrings").Get<EnvironmentProvider>();
            AzEventGrid = Configuration.GetSection("AzureEventGrid").Get<AzureEventGrid>();
             
            Log.Logger = new LoggerConfiguration()
                        .MinimumLevel.Error()
                        .MinimumLevel.Override("Microsoft", new LoggingLevelSwitch(LogEventLevel.Error))
                        .Enrich.WithExceptionDetails(
                            new DestructuringOptionsBuilder()
                                .WithDefaultDestructurers()
                                .WithDestructurers(new ExceptionDestructurer[] { new SqlExceptionDestructurer(), new DbUpdateExceptionDestructurer() })
                        )
                        .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri(Configuration["ElasticConfiguration:Uri"]))
                        {
                            AutoRegisterTemplate = true
                        })
                    .CreateLogger();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        /// <summary>
        /// ConfigureServices
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var hcBuilder = services.AddHealthChecks();

            List<string> eTypes = new List<string>();

            foreach (var _envType in EnvTypes) {

                eTypes.Add(_envType.EnvironmentType.ToString());

                hcBuilder.AddSqlServer(
                DbConnect.GetEnvironmentConnectionString(EnvironmentType.Azure),
                name: $"{_envType.EnvironmentType}DB-check",
                tags: eTypes
               );
            }

            services.AddApplicationInsightsTelemetry(Configuration);

            services.AddMvc(options =>
            {
                options.Filters.Add(new AppExceptionFilterFactory());

            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "DTP Integration Service",
                    Description = "This service acts as a mediator/handler between the ServiceStack/Netcore and ServiceProvider domains"
                });

                options.DescribeAllEnumsAsStrings();
            });

            services.AddMediatR(typeof(Startup));

            services.Configure<IISServerOptions>(options =>
            {
                options.AutomaticAuthentication = false;
            });

            var builder = new ContainerBuilder();


            // DI registration
            builder.RegisterModule(new ServiceRegistry(DbConnect, AzEventGrid, EnvTypes));

            builder.Populate(services);
            this.ApplicationContainer = builder.Build();

            return new AutofacServiceProvider(this.ApplicationContainer);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// <summary>
        /// Configure
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="appLifetime"></param>
        /// <param name="loggerFactory"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime appLifetime, ILoggerFactory loggerFactory)
        {

            loggerFactory.AddSerilog();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCatalogueDiscoveryMiddleware();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("../swagger/v1/swagger.json", "ServiceProvider API V1");
                //c.RoutePrefix = string.Empty;
            });


            app.UseHttpsRedirection();

            var option = new RewriteOptions();
            option.AddRedirect("^$", "swagger");
            app.UseRewriter(option);

            app.UseMvc();

            app.UseHealthChecks("/health", new HealthCheckOptions()
            {
                Predicate = _ => true,
                ResponseWriter = SPHealthCheck.WriteResponse
            });

            appLifetime.ApplicationStopped.Register(() => this.ApplicationContainer.Dispose());
        }
    }
}
