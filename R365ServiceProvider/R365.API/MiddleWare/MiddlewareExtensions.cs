﻿using Microsoft.AspNetCore.Builder;

namespace R365.API.MiddleWare
{
    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder UseCatalogueDiscoveryMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CatalogueDiscovery>();
        }
    }
}
