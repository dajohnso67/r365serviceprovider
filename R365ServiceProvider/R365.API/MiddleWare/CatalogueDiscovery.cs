﻿using LazyCache;
using Microsoft.ApplicationInsights;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.EventGrid.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using R365.Common;
using R365.Domain;
using R365.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace R365.API.MiddleWare
{

    public class CustomerCatalogue {

        public string CustomerInstance { get; set; }
        public string SQLServer { get; set; }
        public string AppPath { get; set; }
    }

    public class CatalogueDiscovery
    {
        private readonly RequestDelegate _next;
        private readonly DbExecute _exec;
        private readonly ConnectionStringProvider _dbConnect;
        private readonly EnvironmentProvider _environments;
        private IAppCache _appCache { get; set; }
        private static TimeSpan cacheExpiry = new TimeSpan(12, 0, 0); //12 hours
        private static string _cacheKey = "customerInstanceKey";
        private AzureEventGrid _azEventGrid { get; set; }
        private readonly TelemetryClient _insightsClient;

        public CatalogueDiscovery(RequestDelegate next, DbExecute exec, IAppCache appCache, AzureEventGrid azEventGrid, ConnectionStringProvider dbConnect, EnvironmentProvider environments, TelemetryClient insightsClient)
        {
            _next = next;
            _exec = exec;
            _appCache = appCache;
            _azEventGrid = azEventGrid;
            _dbConnect = dbConnect;
            _environments = environments;
            _insightsClient = insightsClient;
        }

        /// <summary>
        /// InvokeAsync
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task InvokeAsync(HttpContext context)
        {
            string _path = context.Request.Path.ToString();

            if (_path.ContainsIgnoreCase("/get"))
            {
                string customerName = context.Request.Query["CustomerName"];
                await SetEnvironmentContext(customerName, context);
            }

            if (_path.ContainsIgnoreCase("/run"))
            {
                IHeaderDictionary headers = context.Request.Headers;

                //make sure this is actually originating from a known published event subscription
                if (headers.ContainsKey("Aeg-Subscription-Name") && headers["Aeg-Subscription-Name"].ToString().EqualsIgnoreCase(_azEventGrid.EventName))
                {
                    context.Request.EnableRewind();

                    using (var mem = new MemoryStream())
                    {
                        using (var reader = new StreamReader(mem))
                        {
                            context.Request.Body.CopyTo(mem);

                            mem.Seek(0, SeekOrigin.Begin);

                            string content = reader.ReadToEnd();

                            if (!string.IsNullOrEmpty(content))
                            {
                                context.Request.Body.Position = 0;

                                EventGridEvent egEvent = JsonConvert.DeserializeObject<EventGridEvent[]>(content)[0];

                                if (string.Equals(egEvent.EventType, _azEventGrid.ValidationEventType, StringComparison.OrdinalIgnoreCase))
                                {
                                    await context.CreateValidationResponse(egEvent);
                                    return;
                                }
                                else
                                {
                                    //make sure the id matches the topic subscription Id -- this will enforce publisher/subscriber coordination
                                    if (egEvent.Id.EqualsIgnoreCase(_azEventGrid.SubscriptionId))
                                    {
                                        if (((JObject)egEvent.Data).Count > 0)
                                        {
                                            var data = JsonConvert.SerializeObject(egEvent.Data);

                                            JObject parent = JObject.Parse(data);

                                            string customerName = parent.SelectToken("Catalogue.CustomerName").ToString();

                                            _insightsClient.TrackEvent(egEvent.EventType,

                                               new Dictionary<string, string>()
                                                     {
                                                         { "Catalogue.CustomerName", customerName },
                                                         { "EventGridEvent.EventTime", egEvent.EventTime.ToString("MM/dd/yyyy hh:mm tt") }
                                                     }
                                               );

                                            await SetEnvironmentContext(customerName, context);
                                        }
                                    }
                                    else
                                    {
                                        string errorMessage = "Invalid Publishing Provider Origin";
                                        await HandleError.WriteError(errorMessage, context);
                                        throw new Exception(errorMessage);
                                    }
                                }
                            }
                        }
                    }
                }
                else {

                    string errorMessage = "Invalid Publishing Provider Origin";
                    await HandleError.WriteError(errorMessage, context);
                    throw new Exception(errorMessage);
                }
            }

            await _next(context);
        }

        private async Task SetEnvironmentContext(string customerName, HttpContext context) {

            string custKey = _cacheKey + "_" + customerName;

            CustomerCatalogue _custCatalogue = await _appCache.GetOrAddAsync(custKey, async () =>
            {
                try
                {
                    var parameters = new { CustomerInstance = customerName };
                    string sql = "SELECT TOP(1) CustomerInstance, SQLServer, AppPath FROM CustomerVersion WHERE CustomerInstance = @CustomerInstance";

                    foreach (var envItem in _environments) {

                        _exec.EnvironmentType = envItem.EnvironmentType;
                        List<CustomerCatalogue> cust = await _exec.QueryAsync<CustomerCatalogue>(sql, commandType: System.Data.CommandType.Text, param: parameters);

                        if (cust.Count > 0) { return cust.FirstOrDefault<CustomerCatalogue>(); }
                        
                    }

                    return default;
                }
                catch (Exception ex) {

                    await HandleError.WriteError(ex.Message, context);
                    throw ex;
                }

            }, cacheExpiry);


            if (_custCatalogue != null && !string.IsNullOrEmpty(_custCatalogue.AppPath))
            {
                var env = _custCatalogue.AppPath;

                EnvironmentType environment = (env.EqualsIgnoreCase("loa")) ? EnvironmentType.SoftLayer : EnvironmentType.Azure;

                _exec.EnvironmentType = environment;

                string urlType = (environment == EnvironmentType.Azure) ? "com" : "net";

                context.Items.Add("url_Type", urlType);

                _dbConnect.SetConnectionString(environment);

            }
            else {

                string errorMessage = $"'{customerName}' does not exist in customer catalogue";

                await HandleError.WriteError(errorMessage, context);

                throw new Exception(errorMessage);
            }
        }
    }



    /// <summary>
    /// HandleError
    /// </summary>
    public static class HandleError {

        /// <summary>
        /// WriteError
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static async Task WriteError(string errorMessage, HttpContext context)
        {
            context.Response.StatusCode = 500;
            context.Response.ContentType = "application/json";
            context.Response.Headers.Add("exception", "messageException");

            var json = JsonConvert.SerializeObject(new { Message = errorMessage });

            await context.Response.WriteAsync(json);
        }

    }


    /// <summary>
    /// HttpContextExtensions
    /// </summary>
    public static class HttpContextExtensions
    {
        /// <summary>
        /// CreateValidationResponse
        /// </summary>
        /// <param name="context"></param>
        /// <param name="eventGridEvent"></param>
        /// <returns></returns>
        public static async Task CreateValidationResponse(this HttpContext context, EventGridEvent eventGridEvent)
        {
            var data = eventGridEvent.Data as JObject;
            var eventData = data.ToObject<SubscriptionValidationEventData>();
            var responseData = new SubscriptionValidationResponse
            {
                ValidationResponse = eventData.ValidationCode
            };

            context.Response.ContentType = "application/json";
            JsonResult output = new JsonResult(responseData);

            string _out = JsonConvert.SerializeObject(output.Value);

            await context.Response.WriteAsync(_out);
        }
    }
}
