﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using R365.Common;
using System.IO;

namespace R365.API.MiddleWare
{
    public class RequestType<T>
    {
        public T output { get; private set; }

        public RequestType(HttpContext context, EnvelopeType envelopeType)
        {
            context.Request.EnableRewind();

            using (var mem = new MemoryStream())
            {
                using (var reader = new StreamReader(mem))
                {
                    context.Request.Body.CopyTo(mem);

                    mem.Seek(0, SeekOrigin.Begin);

                    string content = reader.ReadToEnd();

                    if (!string.IsNullOrEmpty(content))
                    {
                        context.Request.Body.Position = 0;

                        JObject parent = JObject.Parse(content);

                        JToken envType = parent["envelope"][envelopeType.ToString()];
                        string catalogue = envType.ToString(Formatting.None);

                        output = JsonConvert.DeserializeObject<T>(catalogue);
                    }
                }
            }
        }
    }
}
