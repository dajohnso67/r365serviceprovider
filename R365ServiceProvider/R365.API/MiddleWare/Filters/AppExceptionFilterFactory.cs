﻿using Microsoft.ApplicationInsights;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace R365.API.MiddleWare.Filters
{
    public class AppExceptionFilterFactory : IFilterFactory
    {
        public bool IsReusable => false;

        /// <summary>
        /// CreateInstance
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <returns>IFilterMetadata</returns>
        public IFilterMetadata CreateInstance(IServiceProvider serviceProvider)
        {
            //var logger = (ILoggerFactory)serviceProvider.GetService(typeof(ILoggerFactory));
            var env = (IHostingEnvironment)serviceProvider.GetService(typeof(IHostingEnvironment));
            var insightsClient = (TelemetryClient)serviceProvider.GetService(typeof(TelemetryClient));

            return new AppExceptionFilter(env, insightsClient);
        }
    }
}
