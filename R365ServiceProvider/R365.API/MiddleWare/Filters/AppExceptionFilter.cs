﻿using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Serilog;
using System;

namespace R365.API.MiddleWare.Filters
{
    public class AppExceptionFilter : ExceptionFilterAttribute
    {

        private readonly IHostingEnvironment _env;
        private readonly TelemetryClient _insightsClient;

        public AppExceptionFilter(IHostingEnvironment env, TelemetryClient insightsClient)
        {
            _env = env;
            _insightsClient = insightsClient;
        }

        public override void OnException(ExceptionContext context) {

            Log.Error(context.Exception, "R365ServiceProvider Exception");

            _insightsClient.TrackException(
                new ExceptionTelemetry
                {
                    Exception = context.Exception,
                    Message = $"({_env.EnvironmentName}) -- {context.Exception.Message}",
                    Timestamp = DateTime.Now
                }
             );

            context.HttpContext.Response.ContentType = "application/json";

            context.Result = new JsonResult(
                new
                {
                    error = new
                    {
                        path = context.HttpContext.Request.Path,
                        method = context.HttpContext.Request.Method,
                        status = context.Exception.GetType().Name,
                        source = context.Exception.Source,
                        message = context.Exception.Message.Replace("\r\n", " "),
                        stacktrace = context.Exception.StackTrace.Replace("\r\n", " "),
                        environment = _env.EnvironmentName
                    }
                });
        }
    }
}
