﻿using System;
using System.ComponentModel.DataAnnotations;

namespace R365.Domain
{

    public class ServiceProviderUser
    {
        [Key]
        public int? Id { get; set; }
        public Guid UserId { get; set; }
        public string FullName { get; set; }
        public string EmailAddress { get; set; }
        public string UserLogin { get; set; }
        public string CustomerName { get; set; }
    }
}
