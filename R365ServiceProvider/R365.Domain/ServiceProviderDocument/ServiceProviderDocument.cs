﻿using System;
using System.ComponentModel.DataAnnotations;

namespace R365.Domain
{

    public class ServiceProviderDocument
    {
        [Key]
        public int? Id { get; set; }
        public int? FileId { get; set; }
        public Guid? LocationId { get; set; }
        public Guid? AssignedTo { get; set; }
        public string Status { get; set; }
        public string NewFileName { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        public Guid? UserId { get; set; }
        public string CustomerName { get; set; }
    }

}
