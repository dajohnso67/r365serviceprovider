﻿using System.ComponentModel.DataAnnotations;

namespace R365.Domain
{
    public class ServiceProviderCustomer
    {
        [Key]
        public int? Id { get; set; }
        public int? ServiceProviderId { get; set; }
        public string CustomerName { get; set; }
        public string Url { get; set; }
        public bool? SPEnabled { get; set; }
    }
}
