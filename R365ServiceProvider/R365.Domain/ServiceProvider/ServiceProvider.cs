﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace R365.Domain
{
    public class ServiceProvider
    {
        [Key]
        public int? ServiceProviderId { get; set; }
        public string ServiceProviderName { get; set; }
      
    }
}
