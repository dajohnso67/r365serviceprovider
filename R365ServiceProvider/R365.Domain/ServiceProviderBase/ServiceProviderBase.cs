﻿
namespace R365.Domain
{
    public abstract class ServiceProviderBase
    {
        public EnvironmentCatalogue Catalogue { get; set; }

        public ServiceProviderBase() {

            Catalogue = new EnvironmentCatalogue();
        }
    }

    public class EnvironmentCatalogue
    {
        public string CustomerName { get; set; }
    }

}
