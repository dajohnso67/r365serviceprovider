﻿using System;
using System.ComponentModel.DataAnnotations;

namespace R365.Domain
{
    public class ServiceProviderCustomerUser
    {
        [Key]
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public Guid LocationId { get; set; }
        public string CustomerName { get; set; }

    }
}
